_SETTINGS:SetPlayerMenuOff()

-- Place a S-3B Tanker on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local tankerLincoln=RECOVERYTANKER:New("USS Lincoln", "Shell Lincoln @IFF:7463FR") -- Note Az 2020-12-07: InitKeepUnitNames(KeepUnitNames) doesn't want to work here, so placing the @IFF in the groupname is a workaround (as unitname = groupname when spawned by Airboss) 
tankerLincoln:SetTakeoffAir()
tankerLincoln:SetAltitude(9000)
tankerLincoln:SetRadio(138.300) -- set recovery tanker radio frequency
tankerLincoln:SetModex(511)
tankerLincoln:SetTACAN(21,"SL2") -- set recovery tanker TACAN (default channel will be Y)
tankerLincoln:SetCallsign(3,1) -- 3 = Shell name for Tankers (refer to https://wiki.hoggitworld.com/view/DCS_command_setCallsign); 2 = will be the second group of Shell; so callsign in DCS will be Shell 2-1
tankerLincoln:__Start(1)

local awacsLincoln=RECOVERYTANKER:New("USS Lincoln", "Wizard Lincoln @IFF:7462FR")
awacsLincoln:SetCallsign(CALLSIGN.AWACS.Wizard, 1)
awacsLincoln:SetTakeoffAir()
awacsLincoln:SetAltitude(26000)
awacsLincoln:SetRadio(262)
awacsLincoln:SetTACAN(25, "WIZ")
awacsLincoln:__Start(1)

-- Place 1 UH-1H live on the deck of 1 of the escort ship around the carrier
local pedroLincoln=RESCUEHELO:New("USS Lincoln", "Pedro Lincoln") -- edit "name" to match unit name set in mission editor
pedroLincoln:SetHomeBase(AIRBASE:FindByName("USS Show")) -- edit "name" to match unit name set in mission editor
pedroLincoln:SetModex(24)
pedroLincoln:__Start(1)

local airbossLincoln=AIRBOSS:New("USS Lincoln") -- edit "name" to match unit name set in mission editor
airbossLincoln:SetSoundfilesFolder("Airboss Soundfiles/")
airbossLincoln:SetDespawnOnEngineShutdown()
airbossLincoln:SetMaxFlightsPerStack(1) -- set maximum flight (section) in each holding stack
airbossLincoln:SetMaxMarshalStacks(20) -- set maximum holding stack in marshal pattern
airbossLincoln:SetMaxSectionSize(4) -- set maximum number of user plane in 1 section, 1 section = 1 flight
airbossLincoln:SetLSORadio(330.200) -- set LSO (Mother) radio frequency
airbossLincoln:SetMarshalRadio(333.200) -- set Marshall radio frequency
airbossLincoln:SetICLS(12) -- set carrier ICLS channel
airbossLincoln:SetTACAN(72,"X","ABL") -- set carrier TACAN channel
airbossLincoln:SetHandleAIOFF()
airbossLincoln:SetDefaultPlayerSkill(AIRBOSS.Difficulty.EASY) -- only affect LSO + grading and result
airbossLincoln:SetAirbossNiceGuy(true)
airbossLincoln:SetEmergencyLandings(true)
--airbossLincoln:SetPatrolAdInfinitum(true)
airbossLincoln:SetCarrierControlledArea(50)
airbossLincoln:SetMenuRecovery(60, 25, true, 0)
airbossLincoln:SetRadioRelayLSO("Relay Lincoln")
airbossLincoln:SetHoldingOffsetAngle(0)
airbossLincoln:Start()

function tankerLincoln:OnAfterStart(From,Event,To)
  airbossLincoln:SetRecoveryTanker(tankerLincoln)
  airbossLincoln:SetAWACS(awacsLincoln)
end

function pedroLincoln:OnAfterStart(From,Event,To)
  airbossLincoln:SetRadioRelayMarshal(self:GetUnitName())
end