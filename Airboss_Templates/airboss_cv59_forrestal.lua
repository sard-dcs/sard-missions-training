_SETTINGS:SetPlayerMenuOff()

-- Place a S-3B Tanker on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local tankerForrestal=RECOVERYTANKER:New("CV59-Carrier-Group-1", "Shell Forrestal @IFF:7462FR") -- Note Az 2020-12-07: InitKeepUnitNames(KeepUnitNames) doesn't want to work here, so placing the @IFF in the groupname is a workaround (as unitname = groupname when spawned by Airboss) 
tankerForrestal:SetTakeoffAir()
tankerForrestal:SetAltitude(9000)
tankerForrestal:SetRadio(138.300) -- set recovery tanker radio frequency
tankerForrestal:SetModex(511)
tankerForrestal:SetTACAN(9,"SL2") -- set recovery tanker TACAN (default channel will be Y)
tankerForrestal:SetCallsign(3,2) -- 3 = Shell name for Tankers (refer to https://wiki.hoggitworld.com/view/DCS_command_setCallsign); 2 = will be the second group of Shell; so callsign in DCS will be Shell 2-1
tankerForrestal:__Start(1)

local awacsForrestal=RECOVERYTANKER:New("CV59-Carrier-Group-1", "Wizard Forrestal @IFF:7462FR")
awacsForrestal:SetCallsign(CALLSIGN.AWACS.Wizard, 1)
awacsForrestal:SetTakeoffAir()
awacsForrestal:SetAltitude(26000)
awacsForrestal:SetRadio(262)
awacsForrestal:SetTACAN(19, "WIZ")
awacsForrestal:__Start(1)

-- Place 1 UH-1H live on the deck of 1 of the escort ship around the carrier
local pedroForrestal=RESCUEHELO:New("CV59-Carrier-Group-1", "Pedro Forrestal") -- edit "name" to match unit name set in mission editor
pedroForrestal:SetHomeBase(AIRBASE:FindByName("CV59-Carrier-Group-5")) -- edit "name" to match unit name set in mission editor
pedroForrestal:SetModex(14)
pedroForrestal:__Start(1)
 

local airbossForrestal=AIRBOSS:New("CV59-Carrier-Group-1") -- edit "name" to match unit name set in mission editor
airbossForrestal:SetSoundfilesFolder("Airboss Soundfiles/")
airbossForrestal:SetDespawnOnEngineShutdown()
airbossForrestal:SetMaxFlightsPerStack(1) -- set maximum flight (section) in each holding stack
airbossForrestal:SetMaxMarshalStacks(20) -- set maximum holding stack in marshal pattern
airbossForrestal:SetMaxSectionSize(4) -- set maximum number of user plane in 1 section, 1 section = 1 flight
airbossForrestal:SetLSORadio(337.200) -- set LSO (Mother) radio frequency
airbossForrestal:SetMarshalRadio(334.200) -- set Marshall radio frequency
airbossForrestal:SetICLS(11) -- set carrier ICLS channel
airbossForrestal:SetTACAN(59,"X","THR") -- set carrier TACAN channel
airbossForrestal:SetHandleAIOFF()
airbossForrestal:SetDefaultPlayerSkill(AIRBOSS.Difficulty.EASY) -- only affect LSO + grading and result
airbossForrestal:SetAirbossNiceGuy(true)
airbossForrestal:SetEmergencyLandings(true)
--airbossForrestal:SetPatrolAdInfinitum(true)
airbossForrestal:SetCarrierControlledArea(50)
airbossForrestal:SetMenuRecovery(60, 25, true, 0)
airbossForrestal:SetRadioRelayLSO("Relay Forrestal")
airbossForrestal:SetHoldingOffsetAngle(0)
airbossForrestal:Start()

function tankerForrestal:OnAfterStart(From,Event,To)
  airbossForrestal:SetRecoveryTanker(tankerForrestal)
  airbossForrestal:SetAWACS(awacsForrestal)
end

function pedroForrestal:OnAfterStart(From,Event,To)
  airbossForrestal:SetRadioRelayMarshal(self:GetUnitName())
end