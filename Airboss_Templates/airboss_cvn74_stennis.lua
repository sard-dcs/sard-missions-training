_SETTINGS:SetPlayerMenuOff()

-- Place a S-3B Tanker on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local tankerStennis=RECOVERYTANKER:New("USS John C. Stennis", "Shell Stennis @IFF:7461FR") -- Note Az 2020-12-07: InitKeepUnitNames(KeepUnitNames) doesn't want to work here, so placing the @IFF in the groupname is a workaround (as unitname = groupname when spawned by Airboss) 
tankerStennis:SetTakeoffAir()
tankerStennis:SetAltitude(9000)
tankerStennis:SetRadio(138.300) -- set recovery tanker radio frequency
tankerStennis:SetModex(511)
tankerStennis:SetTACAN(47,"SL2") -- set recovery tanker TACAN (default channel will be Y)
tankerStennis:SetCallsign(3,1) -- 3 = Shell name for Tankers (refer to https://wiki.hoggitworld.com/view/DCS_command_setCallsign); 2 = will be the second group of Shell; so callsign in DCS will be Shell 2-1
tankerStennis:__Start(1)

local awacsStennis=RECOVERYTANKER:New("USS John C. Stennis", "Wizard Stennis @IFF:7462FR")
awacsStennis:SetCallsign(CALLSIGN.AWACS.Wizard, 1)
awacsStennis:SetTakeoffAir()
awacsStennis:SetAltitude(26000)
awacsStennis:SetRadio(262)
awacsStennis:SetTACAN(45, "WIZ")
awacsStennis:__Start(1)

-- Place 1 UH-1H live on the deck of 1 of the escort ship around the carrier
local pedroStennis=RESCUEHELO:New("USS John C. Stennis", "Pedro Stennis") -- edit "name" to match unit name set in mission editor
pedroStennis:SetHomeBase(AIRBASE:FindByName("USS Mobile Bay")) -- edit "name" to match unit name set in mission editor
pedroStennis:SetModex(44)
pedroStennis:__Start(1)

local airbossStennis=AIRBOSS:New("USS John C. Stennis") -- edit "name" to match unit name set in mission editor
airbossStennis:SetSoundfilesFolder("Airboss Soundfiles/")
airbossStennis:SetDespawnOnEngineShutdown()
airbossStennis:SetMaxFlightsPerStack(1) -- set maximum flight (section) in each holding stack
airbossStennis:SetMaxMarshalStacks(20) -- set maximum holding stack in marshal pattern
airbossStennis:SetMaxSectionSize(4) -- set maximum number of user plane in 1 section, 1 section = 1 flight
airbossStennis:SetLSORadio(340.200) -- set LSO (Mother) radio frequency
airbossStennis:SetMarshalRadio(343.200) -- set Marshall radio frequency
airbossStennis:SetICLS(14) -- set carrier ICLS channel
airbossStennis:SetTACAN(74,"X","JCS") -- set carrier TACAN channel
airbossStennis:SetHandleAIOFF()
airbossStennis:SetDefaultPlayerSkill(AIRBOSS.Difficulty.EASY) -- only affect LSO + grading and result
airbossStennis:SetAirbossNiceGuy(true)
airbossStennis:SetEmergencyLandings(true)
--airbossStennis:SetPatrolAdInfinitum(true)
airbossStennis:SetCarrierControlledArea(50)
airbossStennis:SetMenuRecovery(60, 25, true, 0)
airbossStennis:SetRadioRelayLSO("Relay Stennis")
airbossStennis:SetHoldingOffsetAngle(0)
airbossStennis:Start()

function tankerStennis:OnAfterStart(From,Event,To)
  airbossStennis:SetRecoveryTanker(tankerStennis)
  airbossStennis:SetAWACS(awacsStennis) 
end

function pedroStennis:OnAfterStart(From,Event,To)
  airbossStennis:SetRadioRelayMarshal(self:GetUnitName())
end