_SETTINGS:SetPlayerMenuOff()

-- Place a S-3B Tanker on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local tankerRoosevelt=RECOVERYTANKER:New("USS Theodore Roosevelt", "Shell Roosevelt @IFF:7462FR") -- Note Az 2020-12-07: InitKeepUnitNames(KeepUnitNames) doesn't want to work here, so placing the @IFF in the groupname is a workaround (as unitname = groupname when spawned by Airboss) 
tankerRoosevelt:SetTakeoffAir()
tankerRoosevelt:SetAltitude(9000)
tankerRoosevelt:SetRadio(138.300) -- set recovery tanker radio frequency
tankerRoosevelt:SetModex(511)
tankerRoosevelt:SetTACAN(13,"SL2") -- set recovery tanker TACAN (default channel will be Y)
tankerRoosevelt:SetCallsign(3,2) -- 3 = Shell name for Tankers (refer to https://wiki.hoggitworld.com/view/DCS_command_setCallsign); 2 = will be the second group of Shell; so callsign in DCS will be Shell 2-1
tankerRoosevelt:__Start(1)

local awacsRoosevelt=RECOVERYTANKER:New("USS Theodore Roosevelt", "Wizard Roosevelt @IFF:7462FR")
awacsRoosevelt:SetCallsign(CALLSIGN.AWACS.Wizard, 1)
awacsRoosevelt:SetTakeoffAir()
awacsRoosevelt:SetAltitude(26000)
awacsRoosevelt:SetRadio(262)
awacsRoosevelt:SetTACAN(15, "WIZ")
awacsRoosevelt:__Start(1)

-- Place 1 UH-1H live on the deck of 1 of the escort ship around the carrier
local pedroRoosevelt=RESCUEHELO:New("USS Theodore Roosevelt", "Pedro Roosevelt") -- edit "name" to match unit name set in mission editor
pedroRoosevelt:SetHomeBase(AIRBASE:FindByName("USS Livourne")) -- edit "name" to match unit name set in mission editor
pedroRoosevelt:SetModex(14)
pedroRoosevelt:__Start(1)
 

local airbossRoosevelt=AIRBOSS:New("USS Theodore Roosevelt") -- edit "name" to match unit name set in mission editor
airbossRoosevelt:SetSoundfilesFolder("Airboss Soundfiles/")
airbossRoosevelt:SetDespawnOnEngineShutdown()
airbossRoosevelt:SetMaxFlightsPerStack(1) -- set maximum flight (section) in each holding stack
airbossRoosevelt:SetMaxMarshalStacks(20) -- set maximum holding stack in marshal pattern
airbossRoosevelt:SetMaxSectionSize(4) -- set maximum number of user plane in 1 section, 1 section = 1 flight
airbossRoosevelt:SetLSORadio(337.200) -- set LSO (Mother) radio frequency
airbossRoosevelt:SetMarshalRadio(334.200) -- set Marshall radio frequency
airbossRoosevelt:SetICLS(11) -- set carrier ICLS channel
airbossRoosevelt:SetTACAN(71,"X","THR") -- set carrier TACAN channel
airbossRoosevelt:SetHandleAIOFF()
airbossRoosevelt:SetDefaultPlayerSkill(AIRBOSS.Difficulty.EASY) -- only affect LSO + grading and result
airbossRoosevelt:SetAirbossNiceGuy(true)
airbossRoosevelt:SetEmergencyLandings(true)
--airbossRoosevelt:SetPatrolAdInfinitum(true)
airbossRoosevelt:SetCarrierControlledArea(50)
airbossRoosevelt:SetMenuRecovery(60, 25, true, 0)
airbossRoosevelt:SetRadioRelayLSO("Relay Roosevelt")
airbossRoosevelt:SetHoldingOffsetAngle(0)
airbossRoosevelt:Start()

function tankerRoosevelt:OnAfterStart(From,Event,To)
  airbossRoosevelt:SetRecoveryTanker(tankerRoosevelt)
  airbossRoosevelt:SetAWACS(awacsRoosevelt)
end

function pedroRoosevelt:OnAfterStart(From,Event,To)
  airbossRoosevelt:SetRadioRelayMarshal(self:GetUnitName())
end