-- AI Air-to-Air Dispatcher for Red Coalition
-- Name: AID-BVR-PVE-Init
-- author: c3f59 // Discord ID: <@425783943237730304>
-- Source: AID-A2A-041 - CAP Independent Detection in EWR with Clients
-- Original Author: FlightControl
-- Original Creation Date: 01 Sep 2017

-- Send a debug Message
MESSAGE:New( "A2A_BVR-PVE-Init.lua", 30 ):ToAll()

-- Define a SET_GROUP object that builds a collection of groups that define the EWR network.
-- Here we build the network with all the groups that have a name starting with DF RED AWACS and DF RED EWR.
pvpBvrEwrRed = SET_GROUP:New()
pvpBvrEwrRed:FilterPrefixes( { "PVP-BVR-EWR" } )
pvpBvrEwrRed:FilterStart()

PVP-BVR-Detection-RED = DETECTION_AREAS:New( pvpBvrEwrRed, 49000 )
PVP-BVR-Detection-RED:SetRefreshTimeInterval( 30 )

-- Setup the A2A dispatcher, and initialize it.
pvpBvrDispatcherRed = AI_A2A_DISPATCHER:New( PVP-BVR-Detection-RED )
pvpBvrDispatcherRed:SetDefaultTakeoffInAir()
pvpBvrDispatcherRed:SetDefaultLandingNearAirbase()
pvpBvrDispatcherRed:SetDefaultOverhead( 1 ) -- Set default overhead. When not set, default overhead is 1
pvpBvrDispatcherRed:SetGciRadius( 49000 )
pvpBvrDispatcherRed:SetEngageRadius( 90000 )
pvpBvrDispatcherRed:SetDefaultCapLimit( 2 )
pvpBvrDispatcherRed:SetDefaultCapTimeInterval( 20, 20 ) -- Spawn each 5 minutes.

-- Setup the Tactical Display for debug mode.
pvpBvrDispatcherRed:SetTacticalDisplay( true )

-- ########################################################################################################

-- Define a SET_GROUP object that builds a collection of groups that define the EWR network.
-- Here we build the network with all the groups that have a name starting with DF RED AWACS and DF RED EWR.
ewrGroupRed = SET_GROUP:New()
ewrGroupRed:FilterPrefixes( { "RED AWACS", "RED EWR", "RED SAM" } )
ewrGroupRed:FilterStart()

ewrNetworkRed = DETECTION_AREAS:New( ewrGroupRed, 100000 )
ewrNetworkRed:SetRefreshTimeInterval( 30 )

-- Setup the A2A dispatcher, and initialize it.
a2adDispatcherDamascusRed = AI_A2A_DISPATCHER:New( ewrNetworkRed )
a2adDispatcherDamascusRed:SetDefaultTakeoffInAir()
a2adDispatcherDamascusRed:SetDefaultLandingNearAirbase()
a2adDispatcherDamascusRed:SetDefaultOverhead( 1.2 ) -- Set default overhead. When not set, default overhead is 1
a2adDispatcherDamascusRed:SetGciRadius( 100000 )
a2adDispatcherDamascusRed:SetEngageRadius( 90000 )
a2adDispatcherDamascusRed:SetDefaultCapLimit( 2 )
a2adDispatcherDamascusRed:SetDefaultCapTimeInterval( 300, 300 ) -- Spawn each 5 minutes.

-- Setup the Tactical Display for debug mode.
a2adDispatcherDamascusRed:SetTacticalDisplay( false )