function triggerMissionStart (mflag,popup) -- Mission start via flag
        if mflag ~=nil then trigger.action.setUserFlag(mflag, true) end
        trigger.action.outSoundForCoalition(2 , "radiobeep.ogg" )
        trigger.action.outTextForCoalition(2 , popup , 30 , true)
end

function triggerJTACSouth () -- Random JTAC Zone Mission
        STTS.TextToSpeech("MQ9 is in the air. Wait for laser target information.","254","AM","1.0","JTAC Damascus",2,null,-5,"male","en-US")
        triggerMissionStart (nil,"MQ9 is in the air. Wait for laser target information.")
        build_mission_table('AutoJTACSud')
        rndzn=math.random(1,8)
        spawnZone = "AutoJTAC-Sud-" .. rndzn
        respawnMissionObjectsinZone(targets.AutoJTACSud,spawnZone, true, 100, { offsetRoute = true })
        ctld.JTACAutoLase('AutoJTACSud-MQ9', 1688, false, "all", 1, { freq = "254", mod = "AM", name = "JTAC Damascus" })
end

function triggerJTACNorth () -- Random JTAC Zone Mission
        triggerMissionStart (nil,"MQ9 is in the air. Wait for laser target information.")
        build_mission_table('AutoJTACNord')
        rndzn=math.random(1,8)
        spawnZone = "AutoJTAC-Nord-" .. rndzn
        respawnMissionObjectsinZone(targets.AutoJTACNord,spawnZone, true, 100, { offsetRoute = true })
        ctld.JTACAutoLase('AutoJTACNord-MQ9', 1688, false, "all", 1, { freq = "250", mod = "AM", name = "JTAC Aleppo" })
end

function triggerJTACCyprus () -- Random JTAC Zone Mission
        triggerMissionStart (nil,"MQ9 is in the air. Wait for laser target information.")
        build_mission_table('AutoJTACChypres')
        rndzn=math.random(1,10)
        spawnZone = "AutoJTAC-Chypres-" .. rndzn
        respawnMissionObjectsinZone(targets.AutoJTACChypres,spawnZone, true, 100, { offsetRoute = true })
        ctld.JTACAutoLase('AutoJTACChypres-MQ9', 1688, false, "all", 1, { freq = "270", mod = "AM", name = "JTAC Nicosia" })
end

function triggerJTACLondon () -- Random JTAC Zone Mission
        triggerMissionStart (nil,"MQ9 is in the air. Wait for laser target information.")
        build_mission_table('AutoJTACLondon')
        rndzn=math.random(1,12)
        spawnZone = "AutoJTAC-London-" .. rndzn
        respawnMissionObjectsinZone(targets.AutoJTACChypres,spawnZone, true, 100, { offsetRoute = true })
        ctld.JTACAutoLase('AutoJTACLondon-MQ9', 1688, false, "all", 1, { freq = "257", mod = "AM", name = "JTAC London" })
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------
AG = missionCommands.addSubMenu("Air-Sol", nil)
        M012 = missionCommands.addSubMenu("Auto JTAC", AG, nil) --M012
                missionCommands.addCommand("Sud de Damas, 254MHz AM", M012,triggerJTACSouth) -- AutoJTAC Sud
                missionCommands.addCommand("Ouest de Aleppe, 250 Mhz AM", M012,triggerJTACNorth) -- AutoJTAC Nord
                missionCommands.addCommand("Sud de FARP London, 270 MHz AM", M012,triggerJTACLondon) -- AutoJTAC London
                missionCommands.addCommand("Ouest de Nicosia, 257 MHz AM", M012,triggerJTACCyprus) -- AutoJTAC Chypres
        M001 = missionCommands.addSubMenu("Libération de ville au sud de Damas", AG)
                --missionCommands.addCommand("EASY", M001,function() trigger.action.setUserFlag(1, true) end, nil) -- M001
                missionCommands.addCommand("EASY", M001,triggerMissionStart, 1, popup001) -- M001
                missionCommands.addCommand("MEDIUM", M001,triggerMissionStart, 1001, popup1001) -- M001
                missionCommands.addCommand("HARD", M001,triggerMissionStart, 2001, popup2001) -- M001
        M002 = missionCommands.addSubMenu("Destruction Lanceurs SCUD à Sayqal", AG)
                missionCommands.addCommand("HARD", M002,triggerMissionStart, 2, popup002) -- M002
                missionCommands.addCommand("EVEN HARDER", M002,triggerMissionStart, 2002, popup2002) -- M002
        M003 = missionCommands.addSubMenu("Déstruction Convoi Sud de DAMAS", AG)
                missionCommands.addCommand("EASY", M003,triggerMissionStart, 3, popup003) -- M003
                --missionCommands.addCommand("MEDIUM", AG,triggerMissionStart, 1003, popup1003) -- M003
                --missionCommands.addCommand("HARD", AG,triggerMissionStart, 2003, popup2003) -- M003
        M004 = missionCommands.addSubMenu("Déstruction convoi entre Homs et Hama", AG)
                missionCommands.addCommand("EASY", M004,triggerMissionStart, 4, popup004) -- M004
        M005 = missionCommands.addSubMenu("Chypres: Frappe sur l'aéroport de Ercan", AG)
                missionCommands.addCommand("EASY", M005,triggerMissionStart, 5, popup005) -- M005
        --M006 = missionCommands.addSubMenu("Chypres: Intercepter un convoi MLRS", AG)
        --M007 = missionCommands.addSubMenu("Chypres: Défense avant-Poste UN", AG)
        M008 = missionCommands.addSubMenu("Frappe Base de recherche militaire", AG)
                missionCommands.addCommand("EASY", M008,triggerMissionStart, 8, popup008) -- M008
                missionCommands.addCommand("MEDIUM", M008,triggerMissionStart, 1008, popup1008) -- M008
                missionCommands.addCommand("HARD", M008,triggerMissionStart, 2008, popup2008) -- M008
        M010 = missionCommands.addSubMenu("Frappe Port de Tartus", AG)
                missionCommands.addCommand("EASY", M010,triggerMissionStart, 10, popup010) -- M010
                missionCommands.addCommand("MEDIUM", M010,triggerMissionStart, 1010, popup1010) -- M1010
                missionCommands.addCommand("HARD", M010,triggerMissionStart, 2010, popup2010) -- M2010
        --M011 = missionCommands.addSubMenu("Evacuation de Kharab IshK", AG)
        

--------------------------------------------------------------------------------------------------------------------------------------------------------------
AA = missionCommands.addSubMenu("Air-Air", nil)
        missionCommands.addCommand("Easy BVR: Fighter IA ne se défend pas", AA,function() trigger.action.setUserFlag(12, true) end, nil)
        missionCommands.addCommand("Medium BVR: Fighter IA armé sans ECM", AA,function() trigger.action.setUserFlag(10, true) end, nil)
        missionCommands.addCommand("Hard BVR: Fighter IA armé avec ECM", AA,function() trigger.action.setUserFlag(11, true) end, nil)

--------------------------------------------------------------------------------------------------------------------------------------------------------------
AS = missionCommands.addSubMenu("Anti-Ship", nil)
        M009 = missionCommands.addSubMenu("Déstruction de convoi naval", AS)
                missionCommands.addCommand("EASY", M009,triggerMissionStart, 9, popup009) -- M009
                missionCommands.addCommand("MEDIUM", M009,triggerMissionStart, 1009, popup009) -- M1009
                missionCommands.addCommand("HARD", M009,triggerMissionStart, 2009, popup009) -- M2009

--------------------------------------------------------------------------------------------------------------------------------------------------------------