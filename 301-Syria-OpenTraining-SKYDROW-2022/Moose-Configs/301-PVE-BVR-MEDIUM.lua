-- AI A2A Dispatcher GCI and CAP Template
-- Name: 301-PVE-BVR-EASY.lua
-- author: c3f59 // Discord ID: <@425783943237730304>

-- Send a debug Message
MESSAGE:New( "301-PVE-BVR-MEDIUM.lua", 30 ):ToAll()

-- Set up a squadron for the dispatcher.
PVP-BWR-Dispatcher-RED:SetSquadron( "capH4", AIRBASE.Syria.H4, { "CAP-Mig29S-Medium-1", "CAP-SU27-Medium-1" }, 4 )
PVP-BWR-Dispatcher-RED:SetSquadronGrouping( "capH4", 2 ) -- Number of planes per flight
PVP-BWR-Dispatcher-RED:SetSquadronTakeoffInAir( "capH4", 9000 )
PVP-BWR-Dispatcher-RED:SetSquadronLanding( "capH4", PVP-BWR-Dispatcher-RED.Landing.NearAirbase )
PVP-BWR-Dispatcher-RED:SetSquadronFuelThreshold( "capH4", 0.15 ) -- Go RTB when only 30% of fuel remaining in the tank.
PVP-BWR-Dispatcher-RED:SetSquadronCapInterval( "capH4", 2, 1, 5 ) -- Spawn a total of 2 Flights with a delay of minimum 300 seconds
PVP-BWR-Dispatcher-RED:SetSquadronOverhead("capH4", 1)

-- Set up a CAP for the squadron to execute
PVE_BVR_Zone = ZONE:New( "Cap-Zone-H4" )
PVP-BWR-Dispatcher-RED:SetSquadronCap( "capH4", PVE_BVR_Zone, 4000, 9000, 600, 800, 800, 1200, "BARO" )

