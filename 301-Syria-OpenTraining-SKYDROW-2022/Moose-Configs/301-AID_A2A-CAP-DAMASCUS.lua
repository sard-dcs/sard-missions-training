-- AI A2A Dispatcher GCI and CAP Template
-- Name: 301-PVE-BVR-EASY.lua
-- author: c3f59 // Discord ID: <@425783943237730304>

-- Send a debug Message
MESSAGE:New( "301-AID_A2A-CAP-DAMASCUS.lua", 30 ):ToAll()

-- Set up a squadron for the dispatcher.
a2adDispatcherDamascusRed:SetSquadron( "capMezzeh", AIRBASE.Syria.Mezzeh, { "CAP-Mig29S-Medium-1", "CAP-SU27-Medium-1" }, 4 )
a2adDispatcherDamascusRed:SetSquadronGrouping( "capMezzeh", 2 ) -- Number of planes per flight
a2adDispatcherDamascusRed:SetSquadronTakeoffInAir( "capMezzeh", 9000 )
a2adDispatcherDamascusRed:SetSquadronLanding( "capMezzeh", a2adDispatcherDamascusRed.Landing.NearAirbase )
a2adDispatcherDamascusRed:SetSquadronFuelThreshold( "capMezzeh", 0.30 ) -- Go RTB when only 30% of fuel remaining in the tank.
a2adDispatcherDamascusRed:SetSquadronCapInterval( "capMezzeh", 3, 30, 600 ) -- Spawn a total of 2 Flights with a delay of minimum 300 seconds
a2adDispatcherDamascusRed:SetSquadronOverhead("capMezzeh", 1)

-- Set up a CAP for the squadron to execute
CAP_Zone_DAMAS = ZONE_POLYGON:New( "capMezzeh", GROUP:FindByName( "CAP-Zone-Damascus-1" ) )
a2adDispatcherDamascusRed:SetSquadronCap( "capMezzeh", CAP_Zone_DAMAS, 4000, 9000, 600, 800, 800, 1200, "BARO" )