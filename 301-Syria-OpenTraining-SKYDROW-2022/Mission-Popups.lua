popup001 = [[
--------------------------------------------------
COORDONNEES ZONE M001 Libération de ville au sud de Damas

N 33°09'32.17"   E 36°07'46.51"
N 33° 9.536'   E 36° 7.775'

!! Zone défendue par MANPADS, BMPs et Shilkas

--------------------------------------------------
]]

popup1001 = [[
--------------------------------------------------
COORDONNEES ZONE M001 Libération de ville au sud de Damas

N 33°09'32.17"   E 36°07'46.51"
N 33° 9.536'   E 36° 7.775'

!! Zone défendue par MANPADS, BMPs et Shilkas et un Site SA6

--------------------------------------------------
]]

popup2001 = [[
--------------------------------------------------
COORDONNEES ZONE M001 Libération de ville au sud de Damas

N 33°09'32.17"   E 36°07'46.51"
N 33° 9.536'   E 36° 7.775'

!! Zone défendue par MANPADS, BMPs et Shilkas et 1x SA6 + 1x SA2

--------------------------------------------------
]]


popup002 = [[
--------------------------------------------------
COORDONNEES ZONE M002 Destruction Lanceurs SCUD à Sayqal

N 33°41'56.54"   E 37°13'36.19"
N 33°41.942'   E 37°13.603'

!! Zone défendue par 1x SA6, 1x SA2 et un nombre inconnu de SA15

--------------------------------------------------
]]

popup2002 = [[
--------------------------------------------------
COORDONNEES ZONE M002 Destruction Lanceurs SCUD à Sayqal

N 33°41'56.54"   E 37°13'36.19"
N 33°41.942'   E 37°13.603'

!! Zone défendue par 1x SA6, 2x SA2 et un nombre inconnu de SA15

--------------------------------------------------
]]


popup003 = [[
--------------------------------------------------
COORDONNEES ZONE M003 Déstruction Convoi Sud de DAMAS

N 33°05'12.92"   E 36°16'57.17"
N 33° 5.215'   E 36°16.952'
Direction de trajet: Sud

Nous avons recu mot qu'un convoi de SCUD-B est en route vers la base aérienne de Tha'lah. De là, Tel Aviv sera à portée de tir, ce qui constitue une menace inacceptable.

Un Drone MQ-1A est en poursuite pour désignation de cibles.
Détruisez le convoi avant qu'il n'arrive à la base.

--------------------------------------------------
]]

popup004 = [[
--------------------------------------------------
COORDONNEES ZONE M004 Déstruction convoi entre Homs et Hama

N 35° 9'18"   E 33°30' 8"
N 35° 9.303'   E 33°30.139'

--------------------------------------------------
]]

popup005 = [[
--------------------------------------------------
COORDONNEES ZONE M005 Chypres: Frappe sur l'aéroport de Ercan

N 35° 9'18"   E 33°30' 8"
N 35° 9.303'   E 33°30.139'

--------------------------------------------------
]]

popup006 = [[
--------------------------------------------------
COORDONNEES ZONE M006

N 35° 9'18"   E 33°30' 8"
N 35° 9.303'   E 33°30.139'

--------------------------------------------------
]]

popup007 = [[
--------------------------------------------------
COORDONNEES ZONE M007

N 35° 9'18"   E 33°30' 8"
N 35° 9.303'   E 33°30.139'

--------------------------------------------------
]]

popup008 = [[
--------------------------------------------------
COORDONNEES ZONE M008 Mission base de recherches militaires

Bâtiment #1
N 35°59'16.08"   E 37°24'49.94"
N 35°59.268'   E 37°24.832'
Alt: 1448 ft

Bâtiment #2
N 35°59'18.66"   E 37°24'49.66"
N 35°59.311'   E 37°24.827'
Alt: 1433 ft

Une base de recherches militaires se situe au sud-est d'Aleppe. Détruisez deux bâtiments laboratoires au nord-est de l'installation.
!! SA-6 et quelques ZSU-23-4 Shilka.
--------------------------------------------------
]]

popup1008 = [[
--------------------------------------------------
COORDONNEES ZONE M008 Mission base de recherches militaires

Bâtiment #1
N 35°59'16.08"   E 37°24'49.94"
N 35°59.268'   E 37°24.832'
Alt: 1448 ft

Bâtiment #2
N 35°59'18.66"   E 37°24'49.66"
N 35°59.311'   E 37°24.827'
Alt: 1433 ft

Une base de recherches militaires se situe au sud-est d'Aleppe. Détruisez deux bâtiments laboratoires au nord-est de l'installation.
!! 1x SA2, 1x SA6 et quelques ZSU-23-4 Shilka.
--------------------------------------------------
]]

popup2008 = [[
--------------------------------------------------
COORDONNEES ZONE M008 Mission base de recherches militaires

Bâtiment #1
N 35°59'16.08"   E 37°24'49.94"
N 35°59.268'   E 37°24.832'
Alt: 1448 ft

Bâtiment #2
N 35°59'18.66"   E 37°24'49.66"
N 35°59.311'   E 37°24.827'
Alt: 1433 ft

Une base de recherches militaires se situe au sud-est d'Aleppe. Détruisez deux bâtiments laboratoires au nord-est de l'installation.
!! 1x SA6, au moins 1x SA2, quelques ZSU-23-4 Shilka et possible couverture aérienne
--------------------------------------------------
]]

popup009 = [[
--------------------------------------------------
COORDONNEES ZONE M009 Déstruction convoi naval

Départ du Port de Latakia
N 35°31'37.31"   E 35°44'44.48"
N 35°31.621'   E 35°44.741'

Un convoi naval Syrien est parti ravitallier les troupes envahissentes russes en Chypre. Détruisez les tankers 

--------------------------------------------------
]]

popup010 = [[
--------------------------------------------------
COORDONNEES ZONE M0010 Frappe au port de Tartus

Départ du Port de Tartus
N 34°54'22.13"   E 35°52'3.24"
N 34°54.368'   E 35°52.054'

Une flotte russe est arrivée au port de Tartus pour maintenance et ravitaillement.
Détruisez-la pendant ce moment opportun!

--------------------------------------------------
]]

popup1010 = [[
--------------------------------------------------
COORDONNEES ZONE M0010 Frappe au port de Tartus

Départ du Port de Tartus
N 34°54'22.13"   E 35°52'3.24"
N 34°54.368'   E 35°52.054'

Une flotte russe est arrivée au port de Tartus pour maintenance et ravitaillement.
Détruisez-la pendant ce moment opportun!

!! Défenses Aériennes
1. Le port est protegé par un site SA6.

--------------------------------------------------
]]

popup2010 = [[
--------------------------------------------------
COORDONNEES ZONE M0010 Frappe au port de Tartus

Départ du Port de Tartus
N 34°54'22.13"   E 35°52'3.24"
N 34°54.368'   E 35°52.054'

Une flotte russe est arrivée au port de Tartus pour maintenance et ravitaillement.
Détruisez-la pendant ce moment opportun!

!! Défenses Aériennes
1. Le port est protegé par un site SA6, un site SAM inconnu supplémentaire.
2. Les sites SAM sont protégés par des SA-15
3. Il est possible qu'une corvette active soit sur zone.


--------------------------------------------------
]]


popup0011 = [[
--------------------------------------------------
COORDONNEES ZONE M0011 Evacuation de Kharab IshK

N 36°32'43.31"   E 38°35'12.71"
N 36°32.721'   E 38°35.211'


--------------------------------------------------
]]