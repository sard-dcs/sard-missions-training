local function concatArray(a, b)
    local result = {}
    local n = 0
    for _,v in ipairs(a) do n=n+1; result[n]=v end
    for _,v in ipairs(b) do n=n+1; result[n]=v end
    return result
  end

local iranPolyZone = ZONE_POLYGON:New( "Iran Border", GROUP:FindByName( "IRAN_Borders" ) )

local uae_airbases={AIRBASE.PersianGulf.Al_Dhafra_AB, AIRBASE.PersianGulf.Al_Minhad_AB, AIRBASE.PersianGulf.Liwa_Airbase}
local uae_regional={AIRBASE.PersianGulf.Al_Bateen_Airport, AIRBASE.PersianGulf.Abu_Musa_Island_Airport, AIRBASE.PersianGulf.Sas_Al_Nakheel_Airport, AIRBASE.PersianGulf.Sir_Abu_Nuayr}
local uae_intl={AIRBASE.PersianGulf.Abu_Dhabi_International_Airport, AIRBASE.PersianGulf.Al_Ain_International_Airport, AIRBASE.PersianGulf.Al_Maktoum_Intl, AIRBASE.PersianGulf.Dubai_Intl, AIRBASE.PersianGulf.Fujairah_Intl, AIRBASE.PersianGulf.Ras_Al_Khaimah_International_Airport, AIRBASE.PersianGulf.Sharjah_Intl}
local iran_airbases={AIRBASE.PersianGulf.Bandar_Abbas_Intl, AIRBASE.PersianGulf.Kerman_Airport, AIRBASE.PersianGulf.Lar_Airbase, AIRBASE.PersianGulf.Shiraz_International_Airport, AIRBASE.PersianGulf.Tunb_Island_AFB, AIRBASE.PersianGulf.Kish_International_Airport}
local iran_regional={AIRBASE.PersianGulf.Havadarya, AIRBASE.PersianGulf.Bandar_e_Jask_airfield, AIRBASE.PersianGulf.Bandar_Lengeh, AIRBASE.PersianGulf.Jiroft_Airport, AIRBASE.PersianGulf.Kerman_Airport, AIRBASE.PersianGulf.Khasab, AIRBASE.PersianGulf.Lar_Airbase, AIRBASE.PersianGulf.Lavan_Island_Airport, AIRBASE.PersianGulf.Qeshm_Island, AIRBASE.PersianGulf.Sirri_Island, AIRBASE.PersianGulf.Tunb_Kochak}
local iran_intl={AIRBASE.PersianGulf.Bandar_Abbas_Intl, AIRBASE.PersianGulf.Kish_International_Airport, AIRBASE.PersianGulf.Shiraz_International_Airport}



local iriaf_C130=RAT:New("RAT_IRIAF_C130")
iriaf_C130:SetCoalition("sameonly")
iriaf_C130:ExcludedAirports(AIRBASE.PersianGulf.Khasab)
iriaf_C130:SetDeparture(iran_airbases)
iriaf_C130:SetDestination(iran_airbases)
iriaf_C130:SetTakeoffRunway()
iriaf_C130:SetTerminalType(AIRBASE.TerminalType.OpenBig)
iriaf_C130:ContinueJourney()

local aeroflot_yak40=RAT:New("RAT_Yak40_Aeroflot")
aeroflot_yak40:SetCoalition("sameonly")
aeroflot_yak40:SetDeparture(iran_regional, iran_intl)
aeroflot_yak40:SetDestination(iran_regional, iran_intl)
aeroflot_yak40:SetTakeoffRunway()
aeroflot_yak40:SetTerminalType(AIRBASE.TerminalType.OpenMedOrBig)
aeroflot_yak40:SetMinDistance(60)
aeroflot_yak40:ContinueJourney()
aeroflot_yak40:Invisible()

local aeroflot_an26=RAT:New("RAT_An26B_Aeroflot")
aeroflot_an26:SetCoalition("same")
aeroflot_an26:SetDeparture(iran_regional, iran_intl)
aeroflot_an26:SetDestination(iran_regional, iran_intl)
aeroflot_an26:SetTakeoffRunway()
aeroflot_an26:SetTerminalType(AIRBASE.TerminalType.OpenMedOrBig)
aeroflot_an26:SetMinDistance(60)
aeroflot_an26:ContinueJourney()
aeroflot_an26:Invisible()

local iran_b737=RAT:New("RAT_B737_Iran")
iran_b737:SetCoalition("same")
iran_b737:Livery({"OMAN AIR", "EA", "Air Algerie"})
iran_b737:SetDeparture(iran_regional, iran_intl, uae_intl, uae_regional)
iran_b737:SetDestination(iran_regional, iran_intl, uae_intl, uae_regional)
iran_b737:SetTakeoffRunway()
iran_b737:SetTerminalType(AIRBASE.TerminalType.OpenMedOrBig)
iran_b737:SetMinDistance(120)
iran_b737:ContinueJourney()
iran_b737:Invisible()

local west_b757=RAT:New("RAT_B757_West")
west_b757:SetCoalition("same")
west_b757:Livery({"British Airways", "DHL Cargo", "Swiss", "Thomson TUI", "easyjet"})
local dep=concatArray({"RAT_IntlSpawn_N", "RAT_IntlSpawn_NW", "RAT_IntlSpawn_SW", "RAT_IntlSpawn_S", "RAT_IntlSpawn_SE"}, uae_intl)
west_b757:SetDeparture(dep)
west_b757:SetDestination(uae_intl)
west_b757:SetTakeoffRunway()
west_b757:SetTerminalType(AIRBASE.TerminalType.OpenBig)
west_b757:SetMinDistance(20)
west_b757:Commute()
west_b757:Invisible()

local iran_mgr=RATMANAGER:New(50)
iran_mgr:Add(iriaf_C130, 1)
iran_mgr:Add(aeroflot_yak40, 5)
iran_mgr:Add(aeroflot_an26, 5)
iran_mgr:Add(iran_b737, 15)
iran_mgr:Add(west_b757, 20)
iran_mgr:Start(1)
iran_mgr:Stop(28800)