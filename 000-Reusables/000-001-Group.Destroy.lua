-- stop condition lua predicate fuel low
local wizfuel = Unit.getByName('Wizard'):getFuel
if wizfuel < 0.85 then return true end
-- alternative
if Unit.getByName('Wizard'):getFuel() < 0.85 then return true end



-- destroy MEDIUM and HARD BVR PVE Groups
for k,v in pairs({"Dogfight-MiG29S-1","Dogfight-MiG29S-2","Dogfight-MiG29S-3","Dogfight-MiG29S-4","Dogfight-MiG29S-5","Dogfight-MiG29S-6"}) do
    local myGroup = Group.getByName(v)
    if myGroup and Group.isExist(myGroup) == true then Group.destroy(myGroup) end
end

-- destroy EASY and HARD BVR PVE Groups
for k,v in pairs({"Dogfight-MiG29S-7","Dogfight-MiG29S-4","Dogfight-MiG29S-5","Dogfight-MiG29S-6"}) do
    local myGroup = Group.getByName(v)
    if myGroup and Group.isExist(myGroup) == true then Group.destroy(myGroup) end
end

-- destroy EASY and MEDIUM BVR PVE Groups
for k,v in pairs({"Dogfight-MiG29S-1","Dogfight-MiG29S-2","Dogfight-MiG29S-3","Dogfight-MiG29S-7"}) do
    local myGroup = Group.getByName(v)
    if myGroup and Group.isExist(myGroup) == true then Group.destroy(myGroup) end
end

-- destroy all BVR PVE Groups
for k,v in pairs({"Dogfight-MiG29S-1","Dogfight-MiG29S-2","Dogfight-MiG29S-3","Dogfight-MiG29S-4","Dogfight-MiG29S-5","Dogfight-MiG29S-6", "Dogfight-MiG29S-7"}) do
    local myGroup = Group.getByName(v)
    if myGroup and Group.isExist(myGroup) == true then Group.destroy(myGroup) end
end

