--[[
    Air 2 Air Refueling Training Mission using MiST and MOOSE.
    Requires MiST, MOOSE, and finglongers.lua
]]
local isDebug100=false --log INFO Messages
local isDebug100Verbose=false --display Debug messages in Game
local version="20240322-184206"
ff.logInfo('AAR-TRAINING.LUA', 'Executing 00-100-AAR-Training.lua ver.' .. version, true)

local tanker= {                   --Place 3 tankers in mission editor with late activation on and group names set as defined here
  "Arco 8-1 267MHz TACAN 1Y",     --KC-135MPRS basket tanker 24000ft at 350 kts GS
  "Texaco 8-1 268MHz TACAN 2Y",   --KC-135 boom tanker high up and fast 24000ft at 350 kts GS
  "Shell 8-1 269MHz TACAN 3Y"     --KC-135 boom tanker low and slow 15000 ft at 265 kts GS
}
local spawnZonesAAR= {"SpawnzoneBasket", "SpawnzoneBoomHiFast", "SpawnzoneBoomLoSlow"} --Place 3 trigger zones in mission editor. You can place client aircraft in these to activate the tankers automatically with the first player spawning in.


--Setup F10 menu to manually respawn the Tankers
local aarTrainingMenu = missionCommands.addSubMenu("AAR Training", nil)
        missionCommands.addCommand("Respawn Arco 8-1 (Basket)", aarTrainingMenu,function() ff.respawnAlive(tanker[1]) end, nil)
        missionCommands.addCommand("Respawn Texaco 8-1 (Boom High Fast)", aarTrainingMenu,function() ff.respawnAlive(tanker[2]) end, nil)  --maybe unwrap the function call within the function?
        missionCommands.addCommand("Respawn Shell 8-1 (Boom Low Slow)", aarTrainingMenu,function() ff.respawnAlive(tanker[3]) end, nil)

--Setup a scheduler to look for units inside the spawnzones
local blueAARTrainingTrigger = SCHEDULER:New(nil,
  function()
    for i, spawnZone in ipairs(spawnZonesAAR) do
      if isDebug100 then ff.logInfo('AAR-TRAINING.LUA', "AAR Training Scheduler running index " .. i .. " for zone " .. spawnZone .. " to spawn " .. tanker[i], isDebug100Verbose) end

      local myZone = ZONE:FindByName(spawnZone)
      local myTanker = Group.getByName(tanker[i]):getUnit(1)
      local clientsInZone = SET_CLIENT:New():FilterCoalitions('blue'):FilterCategories("plane", "helicopter"):FilterZones({myZone}):FilterOnce():CountAlive()
      if not clientsInZone then clientsInZone = 0 end

      if clientsInZone > 0 then
          if Unit.isActive(myTanker) then            
            if isDebug100 then ff.logInfo('AAR-TRAINING.LUA', "Tanker " .. tanker[i] .. " already active. Respawn it manually via the F10 Menu.", isDebug100Verbose) end
          else
            MESSAGE:New( "Spawning tanker " .. tanker[i] , 30 ):ToBlue() --Debug Message
            ff.respawnAlive(tanker[i]) --spawn the tanker
          end
      else
        if isDebug100 then ff.logInfo('AAR-TRAINING.LUA', "No blue clients detected inside " .. spawnZone, isDebug100Verbose) end
      end
      if isDebug100 then ff.logInfo('AAR-TRAINING.LUA', clientsInZone .. " blue clients in zone " .. spawnZone, isDebug100Verbose) end
    end
  --end of scheduler function
  end, {}, 1, 5) --spawnzone trigger scheduler