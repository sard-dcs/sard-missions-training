--[[
    Air 2 Air Refueling Training Mission using MiST and MOOSE.
    Requires MiST, MOOSE, and finglongers.lua
]]
local isDebug101=false --log INFO Messages
local isDebug101Verbose=false --display Debug messages in Game
local version="20240311-155949"
ff.logInfo('DOGFIGHT-TRAINING.LUA', 'Executing 000-101-Dogfight-Training.lua ver.' .. version, true)
local aggressors = {}
  aggressors.active = {}
  aggressors.pool = { { "DGFT-Aggressor-4", "DGFT-Aggressor-5", "DGFT-Aggressor-6" }, { "DGFT-Aggressor-1", "DGFT-Aggressor-2", "DGFT-Aggressor-3" } } --Place any number of aggressor Fighters on the map and copy the group names here
  aggressors.spawnZone = ZONE:FindByName("aggressorSpawnZone") --Place a trigger zone in mission editor. This is where AI aggressors spawn
local dfTrainer = {}
  dfTrainer.zone  = ZONE:FindByName("dogfightTrainingZone") --Place a trigger zone in mission editor. You can place client aircraft in it to activate the aggressors automatically with the first player spawning in.
  dfTrainer.mode = false --set true to activate trainer
  dfTrainer.amount = 1 --amount of aggressor groups to spawn. a group may contain several units
  dfTrainer.difficulty = {1, "Guns only", "Guns + Fox 2"} --table of difficulty settings. first entry is current difficulty level, subsequent are difficulty level names
  dfTrainer.displayStatus=false

local i=1

--Setup F10 menu to manually respawn the Tankers
local dogfightTrainingMenu = missionCommands.addSubMenu("Dogfight Training", nil)
        missionCommands.addCommand("Start dogfight trainer", dogfightTrainingMenu,function() dfTrainer.mode=true end, nil)
        missionCommands.addCommand("Stop dogfight trainer", dogfightTrainingMenu,function() dfTrainer.mode=false end, nil)
        local difficultySubMenu = missionCommands.addSubMenu("Set difficulty", dogfightTrainingMenu)
          missionCommands.addCommand(dfTrainer.difficulty[2], difficultySubMenu,function() dfTrainer.difficulty[1]=1 end, nil)
          missionCommands.addCommand(dfTrainer.difficulty[3], difficultySubMenu,function() dfTrainer.difficulty[1]=2 end, nil)
        local amountSubMenu = missionCommands.addSubMenu("Set amount of AI to spawn", dogfightTrainingMenu)
          missionCommands.addCommand("1", amountSubMenu,function() dfTrainer.amount=1 end, nil)
          missionCommands.addCommand("2", amountSubMenu,function() dfTrainer.amount=2 end, nil)
          missionCommands.addCommand("3", amountSubMenu,function() dfTrainer.amount=3 end, nil)
          missionCommands.addCommand("4", amountSubMenu,function() dfTrainer.amount=4 end, nil)
          missionCommands.addCommand("5", amountSubMenu,function() dfTrainer.amount=5 end, nil)
          missionCommands.addCommand("6", amountSubMenu,function() dfTrainer.amount=6 end, nil)
          missionCommands.addCommand("7", amountSubMenu,function() dfTrainer.amount=7 end, nil)
          missionCommands.addCommand("8", amountSubMenu,function() dfTrainer.amount=8 end, nil)
          missionCommands.addCommand("9", amountSubMenu,function() dfTrainer.amount=9 end, nil)
          missionCommands.addCommand("Display Status", dogfightTrainingMenu,function() dfTrainer.displayStatus=true end, nil)

--Setup a scheduler to look for units inside the spawnzones
local blueDogfightTrainingTrigger = SCHEDULER:New(nil,
  function()

    local clientsInZone = SET_CLIENT:New():FilterCoalitions('blue'):FilterCategories("plane", "helicopter"):FilterZones({dfTrainer.zone}):FilterOnce()
    if isDebug101 then ff.logInfo('DOGFIGHT-TRAINING.LUA', 'Found '.. clientsInZone:CountAlive() ..' clients in zone ' .. dfTrainer.zone:GetName(), isDebug101Verbose) end
    if clientsInZone:CountAlive() == 0 then dfTrainer.mode = false end
    
    local aggressorsInZone = SET_GROUP:New():FilterCoalitions('red'):FilterCategories("plane", "helicopter"):FilterZones({dfTrainer.zone}):FilterOnce()
    if isDebug101 then ff.logInfo('DOGFIGHT-TRAINING.LUA', 'Found '.. aggressorsInZone:CountAlive() ..' aggressors in zone ' .. dfTrainer.zone:GetName(), isDebug101Verbose) end

    -- Dogfight Trainer inactive
    if ( clientsInZone:CountAlive() == 0  or dfTrainer.mode == false ) and aggressorsInZone:CountAlive() > 0 then
      aggressorsInZone:ForEachGroupPartlyInZone(dfTrainer.zone, function(currentGroup) --delete all them groups

        if isDebug101 then ff.logInfo('DOGFIGHT-TRAINING.LUA', 'Trying to destroy group named ' .. currentGroup, isDebug101Verbose) end
        ff.destroyGroups(currentGroup)

      end ) --end of iterator function
      
      MESSAGE:New( 'No players left in dogfight arena Dogfight Trainer deactivated', 20 ):ToAll()

    end

        --Dogfight Trainer active
    if clientsInZone:CountAlive() > 0 and dfTrainer.mode == true then
      MESSAGE:New( 'Dogfight Trainer active', 20 ):ToAll()
      local aggressorsAlive = aggressorsInZone:CountAlive()

      if  aggressorsAlive < dfTrainer.amount then
        if isDebug101 then ff.logInfo('DOGFIGHT-TRAINING.LUA', 'Trying to spawn ' .. dfTrainer.amount-aggressorsAlive .. ' aggressors', isDebug101Verbose) end

        aggressors.active = aggressors.pool[dfTrainer.difficulty[1]] --create set active aggressors pool
        ff.cloneRandomFromList(aggressors.active, dfTrainer.amount-aggressorsAlive, aggressors.spawnZone:GetName()) --spawn them aggressors.active

      end
      
    end

  end, {}, 1, 10) --end of SCHEDULER function