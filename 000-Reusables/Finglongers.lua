ff = {}
ff.debugMessage = function(content, duration, recepients)
    -- print a debug message
        local msg = {}
        msg.text = content
        msg.displayTime = duration
        msg.msgFor = {coa = {recepients}} 
        mist.message.add(msg)
        return true
    end

ff.debugTable = function(table, prefix, suffix)
-- create a debug message listing a given table's content
    local message = ""
    if prefix then message = prefix end
    for k,v in pairs(table) do
        message = message .. k .. " : ".. v .. ", " 
    end
    if suffix then message = message .. suffix end
    ff.debugMessage(message, 10, "all")
    return true
end

ff.filterTableBySubstr = function(tab, substring)
-- returns a table with all values containing substring
    local out_table = {}
    for i, v in ipairs(tab) do
        if string.find(v,substring) then
            table.insert(out_table, v)
        end
    end
    return out_table
end

ff.listCoalitionGroups = function(coa)
-- build table of a given coalition's groups
    local out_table = {}
    for i, v in ipairs(coalition.getGroups(coa)) do
        local gp = Group.getName(v)
        table.insert(out_table, gp)
    end
    return out_table
end

ff.listCoalitionStatics = function(coa)
    -- build table of a given coalition's groups
        local out_table = {}
        for i, v in ipairs(coalition.getStaticObjects(coa)) do
            local st = StaticObject.getName(v)
            table.insert(out_table, st)
        end
        return out_table
    end

ff.spawnMissionObjects = function(groups,statics)
    for i,v in ipairs(groups) do
        local mgroup=mist.getGroupData(v,true)
        mist.dynAdd(mgroup)
    end
    for i,v in ipairs(statics) do
        local mstatic=mist.getGroupData(v)
        mist.dynAddStatic(mstatic)
    end
end

ff.respawnAlive = function(groupName, zoneName) --respawn a group even if it is alive
    if mist.groupIsDead(groupName) == false then
      local myGroup = Group.getByName(groupName)
      Group.destroy(myGroup)
    end
    if zoneName == nil or zoneName == "" then
        mist.respawnGroup(groupName, true)
    else
        mist.respawnInZone(groupName, zoneName)
    end
end

ff.respawnRandomFromList = function(grouptable, count, zoneName) --spawn one or several random groups from table of groupnames
    if count == nil or count < 1 then count = 1 end
    local i=1 
    while i <= count do
        local index=math.random(1,#grouptable)
        if zoneName == nil then 
            ff.respawnAlive(grouptable[index])
        else
            ff.respawnAlive(grouptable[index], zoneName)
        end
        i=i+1
    end
end

ff.cloneRandomFromList = function(grouptable, count, zoneName) --spawn one or several random groups from table of groupnames
    if count == nil or count < 1 then count = 1 end
    local i=1 
    while i <= count do
        local index=math.random(1,#grouptable)
        if zoneName == nil then 
            ff.respawnAlive(grouptable[index])
        else
            --ff.respawnAlive(grouptable[index], zoneName)
            mist.cloneInZone(grouptable[index], zoneName)
        end
        i=i+1
    end
end

ff.logInfo = function(sender,entry,verbose)
    log.write(sender, log.INFO, entry)
    if verbose then MESSAGE:New( entry, 25 ):ToAll() end
end

ff.destroyGroups = function (groupTable)
    if type(groupTable) == "string" then local myGroupList = {groupTable} else myGroupList = groupTable end
    for k,v in pairs(myGroupList) do
        local myGroup = Group.getByName(v)
        if myGroup and Group.isExist(myGroup) == true then Group.destroy(myGroup) end
    end
end

-- ### MAIN CHUNK ### --
do
    local version="20240311-143807"
    log.write('FINGLONGERS.LUA', log.INFO, 'Executing Finglongers.lua ver.' .. version)

    -- Set global list variables
    ff.redCoalitionGroups = ff.listCoalitionGroups(1)
    ff.blueCoalitionGroups = ff.listCoalitionGroups(2)
    ff.redCoalitionStatics = ff.listCoalitionStatics(1)
    ff.blueCoalitionStatics = ff.listCoalitionStatics(2)
end




--[[local mission_groups = ff.filterTableBySubstr(red_coalition_groups, 'Ercan')
local 
local mission_statics = ff.filterTableBySubstr(red_coalition_statics, 'Ercan')
ff.spawnMissionObjects(mission_groups,mission_statics)

ff.debugTable(red_coalition_groups,"red_coalition_groups = ") -- DEBUG 
ff.debugTable(mission_groups,"mission_groups = ") -- DEBUG 
ff.debugTable(red_coalition_statics,"red_coalition_statics = ") -- DEBUG 
ff.debugTable(mission_statics,"mission_statics = ") -- DEBUG 
--]]