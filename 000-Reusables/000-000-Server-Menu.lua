--[[
    Server Command Menu
    Adds an F10 menu item to enable players to load available missions without having access to the server web interface.
    To make this work you need to open <DCS Root>\Scripting\MissionScripting.lua and comment the lines containing sanitizeModule('io') and sanitizeModule('lfs').
    WARNING: this enables scripts to access your file system, network functionality and more, which is a major security risk.
    Use this script at your own discretion.
]]

--local lfs = require ('lfs')
--local DCS = require('DCS')
--local net = require('net')
local serverMenuHook = {}
function serverMenuHook.onSimulationStart()

    local serverMenu = missionCommands.addSubMenu("Server Commands", nil)
    missionCommands.addCommand("Restart this mission", serverMenu,function() net.load_mission(DCS.getMissionFilename()) end, nil)

    local missionDir = lfs.writeDir() .. 'Missions\\'
    print ("current dir is " .. missionDir)
    local missionList = {}
    local fileList = {pcall(lfs.dir, missionDir)}
    if fileList[1] then
        for file in table.unpack(fileList, 2) do
            if string.match(file,".miz") then
                table.insert(missionList,file)
                missionCommands.addCommand("Launch " .. file, serverMenu,function() net.load_mission( missionDir .. file ) end, nil)
            end
        end
    else
        print("error occurred", fileList[2])
    end
end

DCS.setUserCallbacks(serverMenuHook)