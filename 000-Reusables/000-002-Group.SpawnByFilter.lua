function respawnMissionObjectsinZone(area,zone,disperse,radius,options)
    for i,v in ipairs(area.groups) do
        mist.respawnInZone(v,zone,disperse,radius,options)
    end
end

function spawn_mission_objects(area)
    for i,v in ipairs(area.groups) do
        local mgroup=mist.getGroupData(v,true)
        mist.dynAdd(mgroup)
    end
    for i,v in ipairs(area.statics) do
        local mstatic=mist.getGroupData(v)
        mist.dynAddStatic(mstatic)
    end
end

function build_mission_table(mission)
    local mission_filter = {mission}
    for gName, data in pairs(mist.DBs.groupsByName) do
        for i = 1, #mission_filter do
            if string.find(gName, mission_filter[i]) then
                if not targets[mission_filter[i]] then 
                    targets[mission_filter[i]] = {groups = {}, statics = {}}
                end
                if data.category == 'static' then 
                table.insert(targets[mission_filter[i]].statics, gName)
                else
                    table.insert(targets[mission_filter[i]].groups, gName)
                end
            end 
        end
    end
end
--- ### INIT ### ---
targets = {}


-- ### MAIN CHUNK ### --
--build_mission_table('Ercan')
--spawn_mission_objects(targets.Ercan)
--build_mission_table('MRB')
--spawn_mission_objects(targets.MRB)