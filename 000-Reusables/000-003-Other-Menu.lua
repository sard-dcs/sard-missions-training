AG = missionCommands.addSubMenu("Air-Sol", nil)
        missionCommands.addCommand("Easy Strike: Aéroport-IRAK-CAS", AG,function() trigger.action.setUserFlag(1, true) end, nil)
        missionCommands.addCommand("Easy Strike: Base de recherche militaire", AG,function() trigger.action.setUserFlag(38, true) end, nil)
        missionCommands.addCommand("Easy Strike: BRDM 2", AG,function() trigger.action.setUserFlag(8, true) end, nil)
        missionCommands.addCommand("Easy Strike: T72B", AG,function() trigger.action.setUserFlag(9, true) end, nil)
        missionCommands.addCommand("Easy Strike & CAS: Destruction-de-site-sur-DAMASCUS", AG,function() trigger.action.setUserFlag(2, true) end, nil)
        missionCommands.addCommand("Easy CAS & Strike: Ville-aux-mains-de-DAECH-détruire-toutes-présences-ennemies", AG,function() trigger.action.setUserFlag(3, true) end, nil)
        missionCommands.addCommand("Easy CAS: Destruction-convoi-entre-HOMS-et-HAMA", AG,function() trigger.action.setUserFlag(4, true) end, nil)
        missionCommands.addCommand("Easy CAS: Combats OTAN vs EX-PAVA", AG,function() trigger.action.setUserFlag(36, true) end, nil)
        missionCommands.addCommand("Easy CAS: Cannon strike", AG,function() trigger.action.setUserFlag(37, true) end, nil)
        missionCommands.addCommand("Hard DEAD & Strike & CAS: Strike base de recherche militaire", AG,function() trigger.action.setUserFlag(39, true) end, nil)
        missionCommands.addCommand("Hard DEAD: SA11 Palmyra", AG,function() trigger.action.setUserFlag(6, true) end, nil)
        missionCommands.addCommand("Hard CAS: ALEPPE-recherche-objectif", AG,function() trigger.action.setUserFlag(5, true) end, nil)

--------------------------------------------------------------------------------------------------------------------------------------------------------------
AA = missionCommands.addSubMenu("Air-Air", nil)
        missionCommands.addCommand("Easy BVR: Bombardiers sans escorte", AA,function() trigger.action.setUserFlag(35, true) end, nil)
        missionCommands.addCommand("Easy BVR: Fighter IA ne se défend pas", AA,function() trigger.action.setUserFlag(12, true) end, nil)
        missionCommands.addCommand("Medium BVR: Fighter IA armé sans ECM", AA,function() trigger.action.setUserFlag(10, true) end, nil)
        missionCommands.addCommand("Hard BVR: Fighter IA armé avec ECM", AA,function() trigger.action.setUserFlag(11, true) end, nil)

--------------------------------------------------------------------------------------------------------------------------------------------------------------
AS = missionCommands.addSubMenu("Anti-Ship", nil)
        missionCommands.addCommand("Easy: Destruction-naval", AS,function() trigger.action.setUserFlag(7, true) end, nil)

--------------------------------------------------------------------------------------------------------------------------------------------------------------
AGH = missionCommands.addSubMenu("Hélico", nil)
        missionCommands.addCommand("Destruction de l'aéroport de Ercan", AGH,function() trigger.action.setUserFlag(40, true) end, nil)