ff.logInfo("200-a2aDispatcher.lua", "loading 200-a2aDispatcher.lua v." .."20240504160212", true)

--setup detection groups and borders
local ewrIran = SET_GROUP:New():FilterPrefixes( { "iriafAWACS", "iranEW" } ):FilterStart()
local detectionAreaRed = DETECTION_AREAS:New( ewrIran, 30000 ):SetRefreshTimeInterval( 10 )
local borderIran=ZONE_POLYGON:New( "borderIran", GROUP:FindByName( "borderIran" ) )


--setup red a2a dispatcher and initialize it.
local a2adRedEasy = AI_A2A_DISPATCHER:New( detectionAreaRed )
a2adRedEasy:SetBorderZone( borderIran )
a2adRedEasy:SetDefaultOverhead( 1 ) -- Set default overhead. When not set, default overhead is 1
a2adRedEasy:SetGciRadius( 200000 )
a2adRedEasy:SetEngageRadius( 160000 )
a2adRedEasy:SetDefaultCapLimit( 1 )
a2adRedEasy:SetDefaultCapTimeInterval( 300, 300 ) -- Spawn each 5 minutes.

a2adRedEasy:SetSquadron( "iriafF4EKerman", AIRBASE.PersianGulf.Kerman, "iriafF4E" )
a2adRedEasy:SetSquadronGci( "iriafF4EKerman", 350, 400 )
a2adRedEasy:SetSquadronGrouping( "iriafF4EKerman", 3 )

a2adRedEasy:SetSquadron( "iriafF5EBandarAbbas", AIRBASE.PersianGulf.Bandar_Abbas_Intl, "iriafF5E" )
a2adRedEasy:SetSquadronGci( "iriafF5EBandarAbbas", 350, 400 )
a2adRedEasy:SetSquadronGrouping( "iriafF5EBandarAbbas", 3 )

a2adRedEasy:SetSquadron( "iriafMF1Shiraz", AIRBASE.PersianGulf.Shiraz_Intl, "iriafMF1" )
a2adRedEasy:SetSquadronGci( "iriafMF1Shiraz", 350, 400 )
a2adRedEasy:SetSquadronGrouping( "iriafMF1Shiraz", 3 )