-- F10 Menu for Blue Coalition
-- Name: 401-MENU-BLUE-COALITION.lua
-- author: c3f59 // Discord ID: <@425783943237730304>

-- Coalition Top Menu Items
local MenuBlueAA = MENU_COALITION:New( coalition.side.BLUE, "AIR/AIR" )
local MenuBlueAG = MENU_COALITION:New( coalition.side.BLUE, "AIR/SOL" )
local MenuBlueSEAD = MENU_COALITION:New( coalition.side.BLUE, "SEAD/DEAD" )
local MenuBlueSHIP = MENU_COALITION:New( coalition.side.BLUE, "ANTI BATEAU" )
local MenuBlueHELO = MENU_COALITION:New( coalition.side.BLUE, "HELICO" )
local MenuBlueCARGO = MENU_COALITION:New( coalition.side.BLUE, "CARGO" )

-- Define Functions 
local function StartMission( MessageText, MissionFile )
  MESSAGE:New( Coalition, 15 ):ToBlue()
  dofile( MissionFile )
end

-- A/A Menu Commands
local CommandAARota = MENU_COALITION_COMMAND:New( coalition.side.BLUE, "A/A facile sur Rota Island", StartMission, "CAP Rota activé" ,"401-AID_A2A_CAP-RED-EASY.lua" )
local CommandAASaipan = MENU_COALITION_COMMAND:New( coalition.side.BLUE, "A/A dur sur Saipan", StartMission, "GCI Saipan activé", "401-AID_A2A_CAP-RED-HARD.lua" )
-- A/G Menu Commands
-- SEAD Menu Commands
-- SHIP Menu Commands
-- HELICO Menu Commands
-- CARGO Menu Commands