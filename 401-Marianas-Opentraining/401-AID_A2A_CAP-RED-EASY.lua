-- AI A2A Dispatcher GCI and CAP Template
-- Name: 401-AID_A2A_CAP-RED-EASY.lua
-- author: c3f59 // Discord ID: <@425783943237730304>

-- Send a debug Message
MESSAGE:New( "401-AID_A2A_CAP-RED-EASY.lua", 30 ):ToAll()

-- Set up a squadron for the dispatcher.
A2ADispatcher_Red:SetSquadron( "Rota1", AIRBASE.MarianaIslands.Rota_Intl, { "SQ RED Mig-21Bis" }, 4 )
A2ADispatcher_Red:SetSquadronGrouping( "Rota1", 1 ) -- Number of planes per flight
A2ADispatcher_Red:SetSquadronTakeoff( "Rota1", A2ADispatcher_Red.Takeoff.Runway )
A2ADispatcher_Red:SetSquadronLanding( "Rota1", A2ADispatcher_Red.Landing.NearAirbase )
A2ADispatcher_Red:SetSquadronFuelThreshold( "Rota1", 0.30 ) -- Go RTB when only 30% of fuel remaining in the tank.

-- Set up a CAP for the squadron to execute
CAPZoneRota = ZONE_POLYGON:New( "CAP Zone Rota", GROUP:FindByName( "CAP Zone Rota" ) )
A2ADispatcher_Red:SetSquadronCap( "Rota1", CAPZoneRota, 4000, 9000, 600, 800, 800, 1200, "BARO" )
A2ADispatcher_Red:SetSquadronCapInterval( "Rota1", 2, 300, 360 ) -- Spawn a total of 2 Flights with a delay of minimum 300 seconds