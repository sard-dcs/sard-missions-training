-- AI A2A Dispatcher GCI and CAP Template for Red Coalition
-- Name: 401-AID_A2A_CAP-RED-HARD.lua
-- author: c3f59 // Discord ID: <@425783943237730304>

-- Send a debug Message
MESSAGE:New( "401-AID_A2A_CAP-RED-HARD.lua", 30 ):ToAll()

-- Set up a squadron for the dispatcher.
A2ADispatcher_Red:SetSquadron( "Saip1", AIRBASE.MarianaIslands.Saipan_Intl, { "SQ RED SU-27", "SQ RED JF-17" }, 16 )
A2ADispatcher_Red:SetSquadronGrouping( "Saip1", 2 ) -- Number of planes per flight
A2ADispatcher_Red:SetSquadronOverhead( "Saip1", 1 ) -- Set a specific overhead for this squadron
--A2ADispatcher_Red:SetSquadronTakeoff( "Saip1", A2ADispatcher_Red.Takeoff.Runway )
--A2ADispatcher_Red:SetSquadronLanding( "Saip1", A2ADispatcher_Red.Landing.AtRunway )
--A2ADispatcher_Red:SetSquadronFuelThreshold( "Saip1", 0.30 ) -- Go RTB when only 30% of fuel remaining in the tank.

-- Give Squadron to GCI
--A2ADispatcher_Red:SetSquadronGci2( "Saip1", 900, 1200, 2000, 10000, "BARO" )
A2ADispatcher_Red:SetSquadronGci( "Saip1", 900, 1200 )

-- Set up a CAP for the squadron to execute
--CAPZoneRed = ZONE_POLYGON:New( "CAP Zone Red", GROUP:FindByName( "CAP Zone Red" ) )
--A2ADispatcher_Red:SetSquadronCap( "Saip1", CAPZoneRed, 4000, 9000, 600, 800, 800, 1200, "BARO" )
--A2ADispatcher_Red:SetSquadronCapInterval( "Saip1", 2, 180, 300 ) -- Spawn a total of 2 Flights with a delay of minimum 180 seconds