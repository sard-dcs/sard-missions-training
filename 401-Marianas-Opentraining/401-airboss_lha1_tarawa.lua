_SETTINGS:SetPlayerMenuOff()

-- Place 1 UH-1H live on the deck of 1 of the escort ship around the carrier
local pedroTarawa=RESCUEHELO:New("USS Tarawa", "Pedro Tarawa")
pedroTarawa:SetHomeBase(AIRBASE:FindByName("USS Normandy"))
pedroTarawa:SetModex(43)
pedroTarawa:__Start(1)


local airbossTarawa=AIRBOSS:New("USS Tarawa")
airbossTarawa:SetSoundfilesFolder("Airboss Soundfiles/")
airbossTarawa:SetDespawnOnEngineShutdown()
airbossTarawa:SetMaxFlightsPerStack(1)
airbossTarawa:SetMaxMarshalStacks(20)
airbossTarawa:SetMaxSectionSize(4)
airbossTarawa:SetLSORadio(261.000)
airbossTarawa:SetMarshalRadio(262.000)
airbossTarawa:SetICLS(15)
airbossTarawa:SetTACAN(75,"X","TWA")
airbossTarawa:SetHandleAIOFF()
airbossTarawa:SetDefaultPlayerSkill(AIRBOSS.Difficulty.EASY)
airbossTarawa:SetAirbossNiceGuy(true)
airbossTarawa:SetEmergencyLandings(true)
--airbossTarawa:SetPatrolAdInfinitum(true)
airbossTarawa:SetCarrierControlledArea(30)
airbossTarawa:SetMenuRecovery(60, 15, true, 0)
airbossTarawa:SetRadioRelayLSO("Relay Tarawa")
airbossTarawa:SetHoldingOffsetAngle(0)
airbossTarawa:Start()

function pedroTarawa:OnAfterStart(From,Event,To)
  airbossTarawa:SetRadioRelayMarshal(self:GetUnitName())
end

-- --- Function called when a player gets graded by the LSO.
-- function AirbossTarawa:OnAfterLSOGrade(From, Event, To, playerData, grade)
--   local PlayerData=playerData --Ops.Airboss#AIRBOSS.PlayerData
--   local Grade=grade --Ops.Airboss#AIRBOSS.LSOgrade

--   ----------------------------------------
--   --- Interface your Discord bot here! ---
--   ----------------------------------------
  
--   local score=tonumber(Grade.points)
--   local name=tostring(PlayerData.name)
  
--   -- Report LSO grade to dcs.log file.
--   env.info(string.format("Player %s scored %.1f", name, score))
-- end