-- AI Air-to-Air Dispatcher for Red Coalition
-- Name: 401-AID_A2A_INIT
-- author: c3f59 // Discord ID: <@425783943237730304>
-- Source: AID-A2A-041 - CAP Independent Detection in EWR with Clients
-- Original Author: FlightControl
-- Original Creation Date: 01 Sep 2017

-- Send a debug Message
MESSAGE:New( "401-AID_A2A_INIT.lua", 30 ):ToAll()

-- Define a SET_GROUP object that builds a collection of groups that define the EWR network.
-- Here we build the network with all the groups that have a name starting with DF RED AWACS and DF RED EWR.
EWR_Red = SET_GROUP:New()
EWR_Red:FilterPrefixes( { "DF RED AWACS", "DF RED EWR" } )
EWR_Red:FilterStart()

Detection_Red = DETECTION_AREAS:New( EWR_Red, 30000 )
Detection_Red:SetRefreshTimeInterval( 10 )

-- Setup the A2A dispatcher, and initialize it.
A2ADispatcher_Red = AI_A2A_DISPATCHER:New( Detection_Red )
A2ADispatcher_Red:SetDefaultTakeoffInAir()
A2ADispatcher_Red:SetDefaultLandingNearAirbase()
A2ADispatcher_Red:SetDefaultOverhead( 1.2 ) -- Set default overhead. When not set, default overhead is 1
A2ADispatcher_Red:SetGciRadius( 100000 )
A2ADispatcher_Red:SetEngageRadius( 90000 )
A2ADispatcher_Red:SetDefaultCapLimit( 2 )
A2ADispatcher_Red:SetDefaultCapTimeInterval( 300, 300 ) -- Spawn each 5 minutes.

-- Setup the Tactical Display for debug mode.
A2ADispatcher_Red:SetTacticalDisplay( true )
--A2ADispatcher_Red:SetRefreshTimeInterval( 30 )