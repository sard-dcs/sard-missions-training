-- AI A2A Dispatcher GCI and CAP Template
-- Name: 401-AID_A2A_CAP-KUSNETZOV.lua
-- author: c3f59 // Discord ID: <@425783943237730304>

-- Send a debug Message
MESSAGE:New( "401-AID_A2A_CAP-KUSNETZOV.lua", 30 ):ToAll()

-- Set up a squadron for the dispatcher.
A2ADispatcher_Red:SetSquadron( "CapKuz", "Admiral Kuznetsov", { "SQ RED CAP KUSNETZOV SU-33" }, 32 )
A2ADispatcher_Red:SetSquadronGrouping( "CapKuz", 2 ) -- Number of planes per flight
A2ADispatcher_Red:SetSquadronOverhead( "CapKuz", 2 ) -- Set a specific overhead for this squadron
A2ADispatcher_Red:SetSquadronTakeoff( "CapKuz", A2ADispatcher_Red.Takeoff.Air )
A2ADispatcher_Red:SetSquadronLanding( "CapKuz", A2ADispatcher_Red.NearAirbase )
--A2ADispatcher_Red:SetSquadronFuelThreshold( "CapKuz", 0.30 ) -- Go RTB when only 30% of fuel remaining in the tank.

-- Give Squadron to GCI
A2ADispatcher_Red:SetSquadronGci( "CapKuz", 900, 1200 )

-- Set up a CAP for the squadron to execute
Kuz = UNIT:FindByName( "Admiral Kuznetsov" )
CAPZoneKuznetsov = ZONE_UNIT:New("Cap Zone Kuznetsov", Kuz, 24000)
A2ADispatcher_Red:SetSquadronCap( "CapKuz", CAPZoneKuznetsov, 4000, 9000, 600, 800, 800, 1200, "BARO" )
A2ADispatcher_Red:SetSquadronCapInterval( "CapKuz", 2, 30, 120 ) -- Spawn a total of 2 Flights with a delay of 30 to 120 seconds