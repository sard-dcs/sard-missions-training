MESSAGE:New( "300-RAT-Syria.lua 028", 25 ):ToAll()

  local function concatArray(a, b)
    local result = {}
    local n = 0
    for _,v in ipairs(a) do n=n+1; result[n]=v end
    for _,v in ipairs(b) do n=n+1; result[n]=v end
    return result
  end


local blue_intl={AIRBASE.Syria.Adana_Sakirpasa, AIRBASE.Syria.Paphos, AIRBASE.Syria.Larnaca}

local turkey_regional={AIRBASE.Syria.Sanliurfa, AIRBASE.Syria.Adana_Sakirpasa, AIRBASE.Syria.Gaziantep, AIRBASE.Syria.Hatay, AIRBASE.Syria.Gazipasa}

local lebanon_airbases={AIRBASE.Syria.Wujah_Al_Hajar, AIRBASE.Syria.Naqoura, AIRBASE.Syria.Beirut_Rafic_Hariri}
local lebanon_regional={AIRBASE.Syria.Rayak, AIRBASE.Syria.Rene_Mouawad}
local lebanon_logisticbases={AIRBASE.Syria.Beirut_Rafic_Hariri}

local israel_airbases={AIRBASE.Syria.Ramat_David, AIRBASE.Syria.Haifa, AIRBASE.Syria.Megiddo, AIRBASE.Syria.Eyn_Shemer}
local israel_regional={AIRBASE.Syria.Kiryat_Shmona, AIRBASE.Syria.Rosh_Pina}

local nato_south_airbases={AIRBASE.Syria.H3, AIRBASE.Syria.H3_Northwest, AIRBASE.Syria.H3_Southwest, AIRBASE.Syria.H4}
local nato_north_airbases={AIRBASE.Syria.Incirlik, AIRBASE.Syria.Akrotiri}
local blue_supportbases={AIRBASE.Syria.Sanliurfa, AIRBASE.Syria.Paphos, AIRBASE.Syria.Kingsfield}


local syria_red_airbases={AIRBASE.Syria.Hama, AIRBASE.Syria.Damascus, AIRBASE.Syria.Tiyas, AIRBASE.Syria.Taftanaz, AIRBASE.Syria.Qabr_as_Sitt, AIRBASE.Syria.Mezzeh, AIRBASE.Syria.Khalkhalah, AIRBASE.Syria.Aleppo, AIRBASE.Syria.Pinarbashi, AIRBASE.Syria.Marj_Ruhayyil, AIRBASE.Syria.Bassel_Al_Assad, AIRBASE.Syria.Marj_as_Sultan_North, AIRBASE.Syria.Marj_as_Sultan_South, AIRBASE.Syria.Abu_al_Duhur, AIRBASE.Syria.An_Nasiriyah, AIRBASE.Syria.Tabqa, AIRBASE.Syria.Hama, AIRBASE.Syria.Palmyra, AIRBASE.Syria.Minakh, AIRBASE.Syria.Jirah, AIRBASE.Syria.Shayrat, AIRBASE.Syria.Tiyas, AIRBASE.Syria.Sayqal, AIRBASE.Syria.Mezzeh, AIRBASE.Syria.Khalkhalah, AIRBASE.Syria.Al_Dumayr, AIRBASE.Syria.Kuweires, AIRBASE.Syria.Marj_Ruhayyil, AIRBASE.Syria.Bassel_Al_Assad, AIRBASE.Syria.Al_Qusayr}
local syria_red_helobases={AIRBASE.Syria.Qabr_as_Sitt, AIRBASE.Syria.Marj_as_Sultan_North, AIRBASE.Syria.Marj_as_Sultan_South}
local syria_regional={AIRBASE.Syria.Bassel_Al_Assad, AIRBASE.Syria.Aleppo, AIRBASE.Syria.Damascus}
local syria_intl={AIRBASE.Syria.Bassel_Al_Assad, AIRBASE.Syria.Damascus}
local syria_blue_helobases={AIRBASE.Syria.Tal_Siman, AIRBASE.Syria.Kharab_Ishk}


local cyprus_red_airbases={AIRBASE.Syria.Gecitkale}
local cyprus_red_helobases={AIRBASE.Syria.Pinarbashi}
local cyprus_blue_regional={AIRBASE.Syria.Paphos, AIRBASE.Syria.Larnaca}
local cyprus_blue_helobases={AIRBASE.Syria.Paphos, AIRBASE.Syria.Akrotiri, AIRBASE.Syria.Lakatamia}

local red_intl={AIRBASE.Syria.Ercan, AIRBASE.Syria.Bassel_Al_Assad, AIRBASE.Syria.Damascus, AIRBASE.Syria.Aleppo, AIRBASE.Syria.Beirut_Rafic_Hariri}

local liveries_camA320_blue={"Al Maha", "Air Asia", "Air Berlin", "Air Berlin FFO", "Air Berlin OLT", "Air Berlin retro", "Air France", "Air Moldova", "Alitalia", "American Airlines", "British Airways", "Cebu Pacific", "Condor", "Delta Airlines", "Easy Jet", "Easy Jet Berlin", "Easy Jet w", "Edelweiss", "Emirates", "Etihad", "Eurowings", "Eurowings BVB09", "Eurowings Europa Park", "Fly Georgia", "Fly Niki", "Frontier", "German Wings", "Gulf Air", "Iberia", "Jet Blue NY", "JetBlue", "jetBlue FDNY", "Kuwait Airways", "Lufthansa", "Lufthansa New", "Qatar", "SAS", "Saudi Gulf", "Saudia", "Small Planet", "Star Alliance", "SWISS", "Thomas Cook", "Tunis Air", "Turkish Airlines", "United", "US Airways", "Vietnam Airlines", "Virgin", "WiZZ"}
local liveries_camA320_red={"Aeroflot", "Aeroflot 1", "Al Maha", "Iran Air", "Kish Air", "Kuwait Airways", "MEA", "Qatar", "S7", "Ural Airlines"}
local liveries_yak40_blue={"Business Jet 01", "Business Jet 02", "Business Jet 03", "Business Jet 04", "Business Jet 05", "Business Jet 06", "Business Jet 07", "Iberia", "KLM", "LOT"}
local liveries_yak_40_red={"aeroflot", "Business Jet 01", "Business Jet 02", "Business Jet 03", "Business Jet 04", "Business Jet 05", "Business Jet 06", "Business Jet 07", "MEA 1992", "MEA 1999", "Med Airways", "Qeshm Air", "Wings of Lebanon 1", "Wings of Lebanon 2", "Wings of Lebanon 3"}


------------------------------------------------------------------------------------------------------------------------------------

local red_mil_c130=RAT:New("RAT_RED_C130")
red_mil_c130:ATC_Messages(false)
red_mil_c130:SetCoalition("sameonly")
red_mil_c130:Livery({"IRIAF 5-8518", "IRIAF 5-8518", "algerian af green"})
red_mil_c130:SetDeparture(syria_red_airbases, syria_intl)
red_mil_c130:SetDestination(syria_red_airbases)
red_mil_c130:SetTakeoffHot()
red_mil_c130:SetTerminalType(AIRBASE.TerminalType.OpenBig)
red_mil_c130:ContinueJourney()

local red_mil_an30m=RAT:New("RAT_RED_An30M", "RAT_RED_An26B")
red_mil_an30m:ATC_Messages(false)
red_mil_an30m:SetCoalition("sameonly")
red_mil_an30m:SetDeparture(syria_red_airbases, syria_intl)
red_mil_an30m:SetDestination(syria_red_airbases)
red_mil_an30m:SetTakeoffHot()
red_mil_an30m:SetTerminalType(AIRBASE.TerminalType.OpenBig)
red_mil_an30m:ContinueJourney()

local red_mil_an26b=RAT:New("RAT_RED_An26B")
red_mil_an26b:ATC_Messages(false)
red_mil_an26b:SetCoalition("sameonly")
red_mil_an26b:SetDeparture(syria_red_airbases, syria_intl)
red_mil_an26b:SetDestination(syria_red_airbases)
red_mil_an26b:SetTakeoffHot()
red_mil_an26b:SetTerminalType(AIRBASE.TerminalType.OpenBig)
red_mil_an26b:ContinueJourney()

local red_mil_mi8=RAT:New("RAT_RED_Mi8")
red_mil_mi8:ATC_Messages(false)
red_mil_mi8:SetCoalition("sameonly")
red_mil_mi8:Livery({"AFAGIR Sand", "Russia Army Weather", "Russia Ambulance (PF)"})
red_mil_mi8:SetDeparture(syria_red_helobases)
red_mil_mi8:SetDestination(syria_red_airbases, syria_red_helobases)
red_mil_mi8:SetTakeoffHot()
red_mil_mi8:SetTerminalType(AIRBASE.TerminalType.HelicopterOnly)
red_mil_mi8:ContinueJourney()

local Red_Civ_Yak40=RAT:New("RAT_Red_Civ_Yak40")
Red_Civ_Yak40:ATC_Messages(false)
Red_Civ_Yak40:SetCoalition("sameonly")
Red_Civ_Yak40:Livery(liveries_yak_40_red)
Red_Civ_Yak40:SetTerminalType(AIRBASE.TerminalType.OpenMedOrBig)
Red_Civ_Yak40:SetDeparture(syria_regional, lebanon_regional, AIRBASE.Syria.Beirut_Rafic_Hariri)
Red_Civ_Yak40:SetDestination(syria_regional, lebanon_regional, AIRBASE.Syria.Beirut_Rafic_Hariri)
Red_Civ_Yak40:SetTakeoffHot()
Red_Civ_Yak40:ContinueJourney()
Red_Civ_Yak40:SetMinDistance(60)

local Red_Civ_A320=RAT:New("RAT_Red_Civ_A320")
Red_Civ_A320:ATC_Messages(false)
Red_Civ_A320:SetCoalition("sameonly")
Red_Civ_A320:Livery(liveries_camA320_red)
Red_Civ_A320:SetTerminalType(AIRBASE.TerminalType.OpenBig)
Red_Civ_A320:SetDeparture(syria_regional, lebanon_regional, AIRBASE.Syria.Beirut_Rafic_Hariri)
Red_Civ_A320:SetDestination(syria_regional, lebanon_regional, AIRBASE.Syria.Beirut_Rafic_Hariri)
Red_Civ_A320:SetTakeoffHot()
Red_Civ_A320:ContinueJourney()
Red_Civ_A320:SetMinDistance(60)

------------------------------------------------------------------------------------------------------------------------------------

local Blue_Civ_Yak40=RAT:New("RAT_Blue_Civ_Yak40")
Blue_Civ_Yak40:ATC_Messages(false)
Blue_Civ_Yak40:SetCoalition("sameonly")
Blue_Civ_Yak40:Livery(liveries_yak40_blue)
Blue_Civ_Yak40:SetTerminalType(AIRBASE.TerminalType.OpenMedOrBig)
Blue_Civ_Yak40:SetDeparture(blue_intl, cyprus_blue_regional, israel_regional, turkey_regional)
Blue_Civ_Yak40:SetDestination(blue_intl, cyprus_blue_regional, israel_regional, turkey_regional)
Blue_Civ_Yak40:SetTakeoffHot()
Blue_Civ_Yak40:ContinueJourney()
Blue_Civ_Yak40:SetMinDistance(60)

local Blue_Civ_A320=RAT:New("RAT_Blue_Civ_A320")
Blue_Civ_A320:ATC_Messages(false)
Blue_Civ_A320:SetCoalition("sameonly")
Blue_Civ_A320:Livery(liveries_camA320_blue)
Blue_Civ_A320:SetTerminalType(AIRBASE.TerminalType.OpenBig)
Blue_Civ_A320:SetDeparture(blue_intl, cyprus_blue_regional, israel_regional, turkey_regional)
Blue_Civ_A320:SetDestination(blue_intl, cyprus_blue_regional, israel_regional, turkey_regional)
Blue_Civ_A320:SetTakeoffHot()
Blue_Civ_A320:ContinueJourney()
Blue_Civ_A320:SetMinDistance(60)

------------------------------------------------------------------------------------------------------------------------------------

local rat_manager=RATMANAGER:New(20)
rat_manager:Add(Blue_Civ_Yak40, 1)
rat_manager:Add(Blue_Civ_A320, 2)
rat_manager:Add(Red_Civ_Yak40, 1)
rat_manager:Add(Red_Civ_A320, 2)
rat_manager:Add(red_mil_c130, 1)
rat_manager:Add(red_mil_an26b, 2)
rat_manager:Add(red_mil_an30m, 2)
rat_manager:Add(red_mil_mi8, 2)
rat_manager:Start()