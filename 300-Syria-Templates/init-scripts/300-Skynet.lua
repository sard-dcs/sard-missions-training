do
    MESSAGE:New( "300-Skynet.lua 18", 25 ):ToAll()
    --create an instance of the IADS
    local redIADS = SkynetIADS:create('SAA IADS')
 
    --redIADS:addEarlyWarningRadarsByPrefix('RED EW')   --add all units with unit name beginning with 'EW' to the IADS
    --redIADS:addSAMSitesByPrefix('RED SAM')    --add all groups begining with group name 'SAM' to the IADS

    --add a command center:
    local commandCenter = StaticObject.getByName('RED Command Center')
    local powerSource = StaticObject.getByName("RED Command Center Power Source")
    redIADS:addCommandCenter(commandCenter):addPowerSource(powerSource)


    --add a power source and a connection node for this EW radar:
    redIADS:addEarlyWarningRadar('RED EW 55G6U-1')
    local powerSource = StaticObject.getByName('RED Mobile Power Unit')
    local connectionNodeEW = StaticObject.getByName('RED Mobile Command Post')
    redIADS:getEarlyWarningRadarByUnitName('RED EW 55G6U-1'):addPowerSource(powerSource):addConnectionNode(connectionNodeEW)

    --[[
    redIADS:addEarlyWarningRadar('')
    local powerSource = StaticObject.getByName('')
    local connectionNodeEW = StaticObject.getByName('')
    redIADS:getEarlyWarningRadarByUnitName(''):addPowerSource(powerSource):addConnectionNode(connectionNodeEW)
    --]]

    --add a connection node to this SA-2 site, and set the option for it to go dark, if it looses connection to the IADS:
    redIADS:addSAMSite('RED SAM SA-2 Damas-1')
    local connectionNode = Unit.getByName('RED Mobile Command Post-1')
    local powerSource = Unit.getByName("RED Mobile Power Unit-1")
    local powerSource2 = Unit.getByName("RED Power Source-7-1")
    redIADS:addSAMSite('RED SHORAD SA-15 Damas-1')
    local pointDefense = redIADS:getSAMSiteByGroupName('RED SHORAD SA-15 Damas-1')
    redIADS:getSAMSiteByGroupName('RED SAM SA-2 Damas-1'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(50):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)

    redIADS:addSAMSite('RED SAM SA-10 Damas')
    local connectionNode = Unit.getByName('RED Connection Node-1')
    local powerSource = Unit.getByName("RED Power Source-1")
    local powerSource2 = Unit.getByName("RED Power Source-6")
    redIADS:getSAMSiteByGroupName('RED SAM SA-10 Damas'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):setGoLiveRangeInPercent(80):setHARMDetectionChance(80)

    redIADS:addSAMSite('RED SAM SA-6 Al Dumayr')
    local connectionNode = Unit.getByName('RED Mobile Command Post-2-1')
    local powerSource = Unit.getByName("RED Mobile Power Unit-2-1")
    redIADS:getSAMSiteByGroupName('RED SAM SA-6 Al Dumayr'):addPowerSource(powerSource):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(50):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)

    redIADS:addSAMSite('RED SAM SA-2 Al Dumayr')
    local connectionNode = Unit.getByName('RED Connection Node-1-1')
    local powerSource = Unit.getByName("RED Power Source-1")
    local powerSource2 = Unit.getByName("RED Power Source-8-1")
    redIADS:getSAMSiteByGroupName('RED SAM SA-2 Al Dumayr'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):setGoLiveRangeInPercent(80):setHARMDetectionChance(50)

    redIADS:addSAMSite('RED SAM SA-6 Al Dumayr-1')
    local connectionNode = Unit.getByName('RED Connection Node-2-1')
    local powerSource = Unit.getByName("RED Power Source-2-1")
    redIADS:addSAMSite('RED SHORAD SA-15 Al Dumayr-3')
    local pointDefense = redIADS:getSAMSiteByGroupName('RED SHORAD SA-15 Al Dumayr-3')
    redIADS:getSAMSiteByGroupName('RED SAM SA-6 Al Dumayr-1'):addPowerSource(powerSource):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(50):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)

    redIADS:addSAMSite('RED SAM SA-6 Al Dumayr-2')
    local connectionNode = Unit.getByName('RED Connection Node-2-1')
    local powerSource = Unit.getByName("RED Power Source-4-1")
    local powerSource2 = Unit.getByName("RED Power Source-5-1")
    redIADS:addSAMSite('RED SHORAD SA-15 Al Dumayr-1')
    local pointDefense = redIADS:getSAMSiteByGroupName('RED SHORAD SA-15 Al Dumayr-1')
    redIADS:getSAMSiteByGroupName('RED SAM SA-6 Al Dumayr-2'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(50):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)

    redIADS:addSAMSite('RED SAM SA-10 Sayqal-1')
    local connectionNode = Unit.getByName('RED Connection Node-3-1')
    local powerSource = Unit.getByName("RED Power Source-9-1")
    local powerSource2 = Unit.getByName("RED Power Source-10-1")
    redIADS:getSAMSiteByGroupName('RED SAM SA-10 Sayqal-1'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):setGoLiveRangeInPercent(80):setHARMDetectionChance(80)

    redIADS:addSAMSite('RED SAM SA-2 Sayqal-3')
    local connectionNode = Unit.getByName('RED Connection Node-4-1')
    local powerSource = Unit.getByName("RED Power Source-11-1")
    local powerSource2 = Unit.getByName("RED Power Source-16-1")
    redIADS:addSAMSite('RED SHORAD SA-15 Sayqal-12')
    local pointDefense = redIADS:getSAMSiteByGroupName('RED SHORAD SA-15 Sayqal-12')
    redIADS:getSAMSiteByGroupName('RED SAM SA-2 Sayqal-3'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(60)

    redIADS:addSAMSite('RED SAM SA-6 Sayqal-2')
    local connectionNode = Unit.getByName('RED Connection Node-4-1')
    local powerSource = Unit.getByName("RED Power Source-18-1")
    local powerSource2 = Unit.getByName("RED Power Source-17-1")
    redIADS:addSAMSite('RED SHORAD SA-15 Sayqal-10')
    local pointDefense = redIADS:getSAMSiteByGroupName('RED SHORAD SA-15 Sayqal-10')
    redIADS:getSAMSiteByGroupName('RED SAM SA-6 Sayqal-2'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(40):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)

    redIADS:addSAMSite('RED SAM SA-2 Sayqal-1')
    local connectionNode = Unit.getByName('RED Connection Node-6-1')
    local powerSource = Unit.getByName("RED Power Source-13-1")
    local powerSource2 = Unit.getByName("RED Power Source-12-1")
    redIADS:addSAMSite('RED SHORAD SA-15 Sayqal-9')
    local pointDefense = redIADS:getSAMSiteByGroupName('RED SHORAD SA-15 Sayqal-9')
    redIADS:getSAMSiteByGroupName('RED SAM SA-2 Sayqal-1'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(60)

    redIADS:addSAMSite('RED SAM SA-2 Sayqal')
    local connectionNode = Unit.getByName('RED Connection Node-8-1')
    local powerSource = Unit.getByName("RED Power Source-15-1")
    local powerSource2 = Unit.getByName("RED Power Source-19-1")
    redIADS:addSAMSite('RED SHORAD SA-15 Sayqal')
    local pointDefense = redIADS:getSAMSiteByGroupName('RED SHORAD SA-15 Sayqal')
    redIADS:getSAMSiteByGroupName('RED SAM SA-2 Sayqal'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(60)

    redIADS:addSAMSite('RED SAM SA-6 Sayqal-1')
    local connectionNode = Unit.getByName('RED Connection Node-8-1')
    local powerSource = Unit.getByName("RED Power Source-20-1")
    local powerSource2 = Unit.getByName("RED Power Source-14-1")
    redIADS:addSAMSite('RED SHORAD SA-15 Sayqal-2')
    local pointDefense = redIADS:getSAMSiteByGroupName('RED SHORAD SA-15 Sayqal-2')
    redIADS:getSAMSiteByGroupName('RED SAM SA-6 Sayqal-1'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(40):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)

    --[[
    redIADS:addSAMSite('')
    local connectionNode = Unit.getByName('')
    local powerSource = Unit.getByName("")
    local powerSource2 = Unit.getByName("")
    local pointDefense = redIADS:getSAMSiteByGroupName('')
    redIADS:getSAMSiteByGroupName(''):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(50):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)
    --]]

    
    --activate the IADS
    redIADS:addRadioMenu() --activate the radio menu to toggle IADS Status output
    redIADS:activate()	



    --setup blue IADS -----------------------------------------------------------------------
    local blueIADS = SkynetIADS:create('NATO IADS')
    --blueIADS:addSAMSitesByPrefix('BLUE SAM')
    --blueIADS:addEarlyWarningRadarsByPrefix('BLUE EW')

    -- setup blue EWRs ---------------------

    blueIADS:addEarlyWarningRadar('Wizard Stennis @IFF:7462FR')
    blueIADS:addEarlyWarningRadar('BLUE EWR-1-1')
    blueIADS:addEarlyWarningRadar('BLUE EWR-2-1')
    

    --[[
    blueIADS:addEarlyWarningRadar('')
    local powerSource = StaticObject.getByName('')
    local connectionNodeEW = StaticObject.getByName('')
    blueIADS:getEarlyWarningRadarByUnitName(''):addPowerSource(powerSource):addConnectionNode(connectionNodeEW)
    --]]

    -- setup blue SAM Sites ---------------------
    blueIADS:addSAMSite('BLUE SAM Patriot-1')
    local connectionNode = Unit.getByName('BLUE SAM Patriot-1-5')
    local powerSource = Unit.getByName("BLUE SAM Patriot-1-2")
    blueIADS:getSAMSiteByGroupName('BLUE SAM Patriot-1'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):setGoLiveRangeInPercent(80):setHARMDetectionChance(50)

    blueIADS:addSAMSite('BLUE SAM Patriot-2')
    local connectionNode = Unit.getByName('BLUE SAM Patriot-2-5')
    local powerSource = Unit.getByName("BLUE SAM Patriot-2-2")
    blueIADS:getSAMSiteByGroupName('BLUE SAM Patriot-2'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):setGoLiveRangeInPercent(80):setHARMDetectionChance(50)

    blueIADS:addSAMSite('BLUE SAM Patriot-3')
    local connectionNode = Unit.getByName('BLUE SAM Patriot-3-5')
    local powerSource = Unit.getByName("BLUE SAM Patriot-3-2")
    blueIADS:getSAMSiteByGroupName('BLUE SAM Patriot-3'):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):setGoLiveRangeInPercent(80):setHARMDetectionChance(50)

    --[[
    blueIADS:addSAMSite('')
    local connectionNode = Unit.getByName('')
    local powerSource = Unit.getByName("")
    local powerSource2 = Unit.getByName("")
    blueIADS:addSAMSite('')
    local pointDefense = blueIADS:getSAMSiteByGroupName('')
    blueIADS:getSAMSiteByGroupName(''):addPowerSource(powerSource):addPowerSource(powerSource2):addConnectionNode(connectionNode):addPointDefence(pointDefense):setGoLiveRangeInPercent(80):setHARMDetectionChance(50):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)
    --]]
   

    -- activate the IADS
    blueIADS:addRadioMenu()
    blueIADS:activate()

end