MESSAGE:New( "300-airboss_cvn74_stennis.lua 003", 25 ):ToAll()

_SETTINGS:SetPlayerMenuOff()


local tankerUnit="Shell Stennis @IFF:7461FR" -- Place a S-3B Tanker on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local pedroUnit="Pedro Stennis" -- Place a helicopter live on the deck of CVN-74-7
local awacsUnit="Wizard Stennis @IFF:7462FR" -- Place a E-2D AWACS on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local relayUnit="Relay Stennis"


-- Place a S-3B Tanker on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local tankerStennis=RECOVERYTANKER:New("CVN-74-1", tankerUnit) -- Note Az 2020-12-07: InitKeepUnitNames(KeepUnitNames) doesn't want to work here, so placing the @IFF in the groupname is a workaround (as unitname = groupname when spawned by Airboss) 
tankerStennis:SetTakeoffAir()
tankerStennis:SetAltitude(6000)
tankerStennis:SetRacetrackDistances(8, 6)
tankerStennis:SetRadio(138.300) -- set recovery tanker radio frequency
tankerStennis:SetModex(511)
tankerStennis:SetTACAN(47,"SL1") -- set recovery tanker TACAN (default channel will be Y)
tankerStennis:SetCallsign(3,2) -- 3 = Shell name for Tankers (refer to https://wiki.hoggitworld.com/view/DCS_command_setCallsign); 2 = will be the second group of Shell; so callsign in DCS will be Shell 2-1
tankerStennis:__Start(1)

-- Place a E-2D AWACS on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local awacsStennis=RECOVERYTANKER:New("CVN-74-1", awacsUnit)
awacsStennis:SetTakeoffAir()
awacsStennis:SetAltitude(26000)
awacsStennis:SetRacetrackDistances(15, 15)
awacsStennis:SetRadio(262)
awacsStennis:SetTACAN(45, "WIZ")
awacsStennis:SetCallsign(CALLSIGN.AWACS.Wizard, 1)
awacsStennis:__Start(1)

local pedroStennis=RESCUEHELO:New("CVN-74-1", pedroUnit)
pedroStennis:SetTakeoffAir()
pedroStennis:SetModex(44)
pedroStennis:SetHomeBase(AIRBASE:FindByName("CVN-74-2"))
pedroStennis:__Start(1)

local airbossStennis=AIRBOSS:New("CVN-74-1")
airbossStennis:SetSoundfilesFolder("Airboss Soundfiles/")
airbossStennis:SetDespawnOnEngineShutdown()
airbossStennis:SetMaxFlightsPerStack(1) -- set maximum flight (section) in each holding stack
airbossStennis:SetMaxMarshalStacks(20) -- set maximum holding stack in marshal pattern
airbossStennis:SetMaxSectionSize(4) -- set maximum number of user plane in 1 section, 1 section = 1 flight
airbossStennis:SetRadioRelayLSO(relayUnit)
airbossStennis:SetLSORadio(340.200) -- set LSO (Mother) radio frequency
airbossStennis:SetMarshalRadio(343.200) -- set Marshall radio frequency
airbossStennis:SetICLS(14) -- set carrier ICLS channel
airbossStennis:SetTACAN(74,"X","STN") -- set carrier TACAN channel
airbossStennis:SetHandleAIOFF()
airbossStennis:SetDefaultPlayerSkill(AIRBOSS.Difficulty.EASY) -- only affect LSO + grading and result
airbossStennis:SetAirbossNiceGuy(true)
airbossStennis:SetEmergencyLandings(true)
airbossStennis:SetCarrierControlledArea(50)
airbossStennis:SetMenuRecovery(60, 25, true, 0)
airbossStennis:SetHoldingOffsetAngle(0)
airbossStennis:Start()

function tankerStennis:OnAfterStart(From,Event,To)
  airbossStennis:SetRecoveryTanker(tankerStennis)
  airbossStennis:SetAWACS(awacsStennis) 
end

function pedroStennis:OnAfterStart(From,Event,To)
  airbossStennis:SetRadioRelayMarshal(self:GetUnitName())
end