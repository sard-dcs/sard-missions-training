ff.logInfo("302-A2A-Dispatcher.lua", "Running 302-A2A-Dispatcher.lua v." .."20240509161203", true)

--setup red a2a dispatcher and initialize it.
local ewrSyriaHard = SET_GROUP:New():FilterPrefixes( { "RED AWACS", "RED EW" } ):FilterStart()
local detectionSyriaHard = DETECTION_AREAS:New( ewrSyriaHard, 30000 ):SetRefreshTimeInterval( 10 )
local borderZoneRedHard=ZONE_POLYGON:New( "Borders_SYRIA-Hard", GROUP:FindByName( "Borders_SYRIA-Hard" ) )
local a2adRedHard = AI_A2A_DISPATCHER:New( detectionSyriaHard )

a2adRedHard:SetBorderZone( borderZoneRedHard )
a2adRedHard:SetDefaultOverhead( 1.5 ) -- Set default overhead. When not set, default overhead is 1
a2adRedHard:SetGciRadius( 200000 )
a2adRedHard:SetEngageRadius( 100000 )
a2adRedHard:SetDefaultCapLimit( 2 )
a2adRedHard:SetDefaultCapTimeInterval( 300, 300 ) -- Spawn each 5 minutes.
a2adRedHard:SetDefaultTakeoffInAir()
a2adRedHard:SetDefaultTakeoffInAirAltitude(300)

a2adRedHard:SetSquadron( "MezzehGCI", AIRBASE.Syria.Mezzeh, { 'SQ RED HARD Mig29S', 'SQ RED HARD Su27', 'SQ RED HARD JF-17', 'SQ RED HARD F-14A' } ):SetSquadronGrouping( "MezzehGCI", 2 )
a2adRedHard:SetSquadronGci( "MezzehGCI", 350, 400 )
a2adRedHard:SetSquadron( "KhalkhalahGCI", AIRBASE.Syria.Khalkhalah, { 'SQ RED HARD Mig29S', 'SQ RED HARD Su27', 'SQ RED HARD JF-17', 'SQ RED HARD F-14A' } ):SetSquadronGrouping( "KhalkhalahGCI", 2 )
a2adRedHard:SetSquadronGci( "KhalkhalahGCI", 350, 400 )
a2adRedHard:SetSquadron( "Marj_RuhayyilGCI", AIRBASE.Syria.Marj_Ruhayyil, { 'SQ RED HARD Mig29S', 'SQ RED HARD Su27', 'SQ RED HARD JF-17', 'SQ RED HARD F-14A' } ):SetSquadronGrouping( "Marj_RuhayyilGCI", 2 )
a2adRedHard:SetSquadronGci( "Marj_RuhayyilGCI", 350, 400 )
a2adRedHard:SetSquadron( "Abu_al_DuhurGCI", AIRBASE.Syria.Abu_al_Duhur, { 'SQ RED HARD Mig29S', 'SQ RED HARD Su27', 'SQ RED HARD JF-17', 'SQ RED HARD F-14A' } ):SetSquadronGrouping( "Abu_al_DuhurGCI", 2 )
a2adRedHard:SetSquadronGci( "Abu_al_DuhurGCI", 350, 400 )
a2adRedHard:SetSquadron( "An_NasiriyahGCI", AIRBASE.Syria.An_Nasiriyah, { 'SQ RED HARD Mig29S', 'SQ RED HARD Su27', 'SQ RED HARD JF-17', 'SQ RED HARD F-14A' } ):SetSquadronGrouping( "An_NasiriyahGCI", 2 )
a2adRedHard:SetSquadronGci( "An_NasiriyahGCI", 350, 400 )
a2adRedHard:SetSquadron( "Tha_lahGCI", AIRBASE.Syria.Tha_lah, { 'SQ RED HARD Mig29S', 'SQ RED HARD Su27', 'SQ RED HARD JF-17', 'SQ RED HARD F-14A' } ):SetSquadronGrouping( "Tha_lahGCI", 2 )
a2adRedHard:SetSquadronGci( "Tha_lahGCI", 350, 400 )


--setup red a2a dispatcher and initialize it.
local ewrSyriaMedium = SET_GROUP:New():FilterPrefixes( { "RED AWACS", "RED EW" } ):FilterStart()
local detectionSyriaMedium = DETECTION_AREAS:New( ewrSyriaMedium, 30000 ):SetRefreshTimeInterval( 10 )
local borderZoneRedMedium=ZONE_POLYGON:New( "Borders_SYRIA-Medium", GROUP:FindByName( "Borders_SYRIA-Medium" ) )
local a2adRedMedium = AI_A2A_DISPATCHER:New( detectionSyriaMedium )

a2adRedMedium:SetBorderZone( borderZoneRedMedium )
a2adRedMedium:SetDefaultOverhead( 1 ) -- Set default overhead. When not set, default overhead is 1
a2adRedMedium:SetGciRadius( 80000 )
a2adRedMedium:SetEngageRadius( 80000 )
a2adRedMedium:SetDefaultCapLimit( 1 )
a2adRedMedium:SetDefaultCapTimeInterval( 300, 300 ) -- Spawn each 5 minutes.
a2adRedMedium:SetDefaultTakeoffInAir()
a2adRedMedium:SetDefaultTakeoffInAirAltitude(300)

a2adRedMedium:SetSquadron( "Bassel_Al_AssadGCI", AIRBASE.Syria.Bassel_Al_Assad, { "SQ RED MED M2000C", "SQ RED MED Mirage F1", "SQ RED MED MiG-29A", "SQ RED MED MiG21Bis" } ):SetSquadronGrouping( "Bassel_Al_AssadGCI", 2 )
a2adRedMedium:SetSquadronGci( "Bassel_Al_AssadGCI", 350, 400 )
a2adRedMedium:SetSquadron( "Al_QusayrGCI", AIRBASE.Syria.Al_Qusayr, { "SQ RED MED M2000C", "SQ RED MED Mirage F1", "SQ RED MED MiG-29A", "SQ RED MED MiG21Bis" } ):SetSquadronGrouping( "Al_QusayrGCI", 2 )
a2adRedMedium:SetSquadronGci( "Al_QusayrGCI", 350, 400 )
a2adRedMedium:SetSquadron( "HamaGCI", AIRBASE.Syria.Hama, { "SQ RED MED M2000C", "SQ RED MED Mirage F1", "SQ RED MED MiG-29A", "SQ RED MED MiG21Bis" } ):SetSquadronGrouping( "HamaGCI", 2 )
a2adRedMedium:SetSquadronGci( "HamaGCI", 350, 400 )
a2adRedMedium:SetSquadron( "PalmyraGCI", AIRBASE.Syria.Palmyra, { "SQ RED MED M2000C", "SQ RED MED Mirage F1", "SQ RED MED MiG-29A", "SQ RED MED MiG21Bis" } ):SetSquadronGrouping( "PalmyraGCI", 2 )
a2adRedMedium:SetSquadronGci( "PalmyraGCI", 350, 400 )
a2adRedMedium:SetSquadron( "ShayratGCI", AIRBASE.Syria.Shayrat, { "SQ RED MED M2000C", "SQ RED MED Mirage F1", "SQ RED MED MiG-29A", "SQ RED MED MiG21Bis" } ):SetSquadronGrouping( "ShayratGCI", 2 )
a2adRedMedium:SetSquadronGci( "ShayratGCI", 350, 400 )
a2adRedMedium:SetSquadron( "TiyasGCI", AIRBASE.Syria.Tiyas, { "SQ RED MED M2000C", "SQ RED MED Mirage F1", "SQ RED MED MiG-29A", "SQ RED MED MiG21Bis" } ):SetSquadronGrouping( "TiyasGCI", 2 )
a2adRedMedium:SetSquadronGci( "TiyasGCI", 350, 400 )


--setup red a2a dispatcher and initialize it.
local ewrSyriaEasy = SET_GROUP:New():FilterPrefixes( { "RED AWACS", "RED EW" } ):FilterStart()
local detectionSyriaEasy = DETECTION_AREAS:New( ewrSyriaEasy, 30000 ):SetRefreshTimeInterval( 10 )
local borderZoneRedEasy=ZONE_POLYGON:New( "Borders_SYRIA-Easy", GROUP:FindByName( "Borders_SYRIA-Easy" ) )
local a2adRedEasy = AI_A2A_DISPATCHER:New( detectionSyriaEasy )

a2adRedEasy:SetBorderZone( borderZoneRedEasy )
a2adRedEasy:SetDefaultOverhead( .5 ) -- Set default overhead. When not set, default overhead is 1
a2adRedEasy:SetGciRadius( 60000 )
a2adRedEasy:SetEngageRadius( 45000 )
a2adRedEasy:SetDefaultCapLimit( 1 )
a2adRedEasy:SetDefaultCapTimeInterval( 300, 300 ) -- Spawn each 5 minutes.
a2adRedEasy:SetDefaultTakeoffInAir()
a2adRedEasy:SetDefaultTakeoffInAirAltitude(300)

a2adRedEasy:SetSquadron( "Abu_al_DuhurGCI", AIRBASE.Syria.Abu_al_Duhur, { 'SQ RED EASY MiG21Bis', 'SQ RED EASY MiG19', 'SQ RED EASY Mirage F1' } ):SetSquadronGrouping( "Abu_al_DuhurGCI", 1 )
a2adRedEasy:SetSquadronGci( "Abu_al_DuhurGCI", 350, 400 )
a2adRedEasy:SetSquadron( "JirahGCI", AIRBASE.Syria.Jirah, { 'SQ RED EASY MiG21Bis', 'SQ RED EASY MiG19', 'SQ RED EASY Mirage F1' } ):SetSquadronGrouping( "JirahGCI", 1 )
a2adRedEasy:SetSquadronGci( "JirahGCI", 350, 400 )
a2adRedEasy:SetSquadron( "KuweiresGCI", AIRBASE.Syria.Kuweires, { 'SQ RED EASY MiG21Bis', 'SQ RED EASY MiG19', 'SQ RED EASY Mirage F1' } ):SetSquadronGrouping( "KuweiresGCI", 1 )
a2adRedEasy:SetSquadronGci( "KuweiresGCI", 350, 400 )
a2adRedEasy:SetSquadron( "TabqaGCI", AIRBASE.Syria.Tabqa, { 'SQ RED EASY MiG21Bis', 'SQ RED EASY MiG19', 'SQ RED EASY Mirage F1' } ):SetSquadronGrouping( "TabqaGCI", 1 )
a2adRedEasy:SetSquadronGci( "TabqaGCI", 350, 400 )