ff.logInfo("302-airboss-setup.lua", "Running 302-airboss-setup.lua v." .."20240510002418", true)

_SETTINGS:SetPlayerMenuOff()

local carrierUnit="CVN-74-1"
local carrierAlias="Stennis"
local heloCarrierUnit="LHA-1-1"
local heloCarrierAlias="Tarawa"
local tankerUnit="Shell Stennis @IFF:7461FR" -- Place a S-3B Tanker on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local awacsUnit="Wizard Stennis @IFF:7462FR" -- Place a E-2D AWACS on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
local relayUnit="Relay Stennis"
local relayUnitLHA="Relay Tarawa"
local pedroUnit="Pedro Stennis" -- Place a helicopter live on the deck of an escort ship

-- Place a E-2D AWACS on the map with late activation checked. Can either be already flying over the carrier group or on the deck of carrier. Note that spawning it on carrier deck can result in very rare bug where AI can't launch
--[[
local awacsStennis=RECOVERYTANKER:New(carrierUnit, awacsUnit)
awacsStennis:SetTakeoffAir()
awacsStennis:SetAltitude(26000)
awacsStennis:SetRacetrackDistances(15, 15)
awacsStennis:SetRadio(262)
awacsStennis:SetTACAN(45, "WIZ")
awacsStennis:SetCallsign(CALLSIGN.AWACS.Wizard, 1)
awacsStennis:__Start(1)
--]]

-- Place a helicopter live on the deck of an escort ship
local pedroStennis=RESCUEHELO:New(carrierUnit, pedroUnit)
pedroStennis:SetTakeoffAir()
pedroStennis:SetModex(44)
pedroStennis:SetHomeBase(AIRBASE:FindByName("CVN-74-2"))
pedroStennis:__Start(1)

--CVN-74 USS John C. Stennis setup
local airbossStennis=AIRBOSS:New(carrierUnit, carrierAlias)
airbossStennis:SetSoundfilesFolder("Airboss Soundfiles/")
airbossStennis:EnableSRS("C:\\DCS-SimpleRadio-Standalone", 5002, "en-US", female)
airbossStennis:SetDespawnOnEngineShutdown()
airbossStennis:SetMaxFlightsPerStack(1) -- set maximum flight (section) in each holding stack
airbossStennis:SetMaxMarshalStacks(20) -- set maximum holding stack in marshal pattern
airbossStennis:SetMaxSectionSize(4) -- set maximum number of user plane in 1 section, 1 section = 1 flight
airbossStennis:SetRadioRelayLSO(relayUnit)
airbossStennis:SetLSORadio(340.200) -- set LSO (Mother) radio frequency
airbossStennis:SetMarshalRadio(343.200) -- set Marshall radio frequency
airbossStennis:SetICLS(14) -- set carrier ICLS channel
airbossStennis:SetTACAN(74,"X","STN") -- set carrier TACAN channel
airbossStennis:SetHandleAIOFF()
airbossStennis:SetDefaultPlayerSkill(AIRBOSS.Difficulty.EASY) -- only affect LSO + grading and result
airbossStennis:SetAirbossNiceGuy(true)
airbossStennis:SetEmergencyLandings(true)
airbossStennis:SetCarrierControlledArea(50)
airbossStennis:SetMenuRecovery(60, 25, true, 0)
airbossStennis:SetHoldingOffsetAngle(0)
airbossStennis:Start()

function tankerStennis:OnAfterStart(From,Event,To)
  airbossStennis:SetRecoveryTanker(tankerStennis)
  airbossStennis:SetAWACS(awacsStennis) 
end

function pedroStennis:OnAfterStart(From,Event,To)
  airbossStennis:SetRadioRelayMarshal(self:GetUnitName())
end



-- LHA-1 USS Tarawa setup
local airbossTarawa=AIRBOSS:New(heloCarrierUnit, heloCarrierAlias)
airbossTarawa:SetSoundfilesFolder("Airboss Soundfiles/")
airbossTarawa:SetDespawnOnEngineShutdown()
airbossTarawa:SetMaxFlightsPerStack(1)
airbossTarawa:SetMaxMarshalStacks(20)
airbossTarawa:SetMaxSectionSize(4)
airbossTarawa:SetLSORadio(333.200) -- set LSO (Mother) radio frequency
airbossTarawa:SetMarshalRadio(330.200) -- set Marshall radio frequency
airbossTarawa:SetICLS(15)
airbossTarawa:SetTACAN(101,"X","TWA")
airbossTarawa:SetHandleAIOFF()
airbossTarawa:SetDefaultPlayerSkill(AIRBOSS.Difficulty.EASY)
airbossTarawa:SetAirbossNiceGuy(true)
airbossTarawa:SetEmergencyLandings(true)
airbossTarawa:SetCarrierControlledArea(30)
airbossTarawa:SetMenuRecovery(60, 15, true, 0)
airbossTarawa:SetRadioRelayLSO(relayUnitLHA)
airbossTarawa:SetHoldingOffsetAngle(0)
airbossTarawa:Start()

--[[
local pedroStennis=RESCUEHELO:New(carrierUnit, pedroUnit)
pedroStennis:SetTakeoffAir()
pedroStennis:SetModex(44)
pedroStennis:SetHomeBase(AIRBASE:FindByName("CVN-74-2"))
pedroStennis:__Start(1)

function pedroTarawa:OnAfterStart(From,Event,To)
  airbossTarawa:SetRadioRelayMarshal(self:GetUnitName())
end

--- Function called when a player gets graded by the LSO.
function AirbossTarawa:OnAfterLSOGrade(From, Event, To, playerData, grade)
  local PlayerData=playerData --Ops.Airboss#AIRBOSS.PlayerData
  local Grade=grade --Ops.Airboss#AIRBOSS.LSOgrade

  ----------------------------------------
  --- Interface your Discord bot here! ---
  ----------------------------------------
  
  local score=tonumber(Grade.points)
  local name=tostring(PlayerData.name)

  -- Report LSO grade to dcs.log file.
  env.info(string.format("Player %s scored %.1f", name, score))
end
--]]